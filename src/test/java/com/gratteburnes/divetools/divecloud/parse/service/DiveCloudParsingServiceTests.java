package com.gratteburnes.divetools.divecloud.parse.service;

import com.gratteburnes.divetools.divecloud.parse.model.ZxuFile;
import com.gratteburnes.divetools.divecloud.parse.model.fsh.Fsh;
import com.gratteburnes.divetools.divecloud.parse.model.zar.Zar;
import com.gratteburnes.divetools.divecloud.parse.model.zdh.Zdh;
import com.gratteburnes.divetools.divecloud.parse.model.zdp.Zdp;
import com.gratteburnes.divetools.divecloud.parse.model.zdp.ZdpRecord;
import com.gratteburnes.divetools.divecloud.parse.model.zdt.Zdt;
import com.gratteburnes.divetools.divecloud.parse.model.zrh.Zrh;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DiveCloudParsingServiceTests {
    public static String TEST_ITEM_FSH = "FSH|^~<>{}|OCI201^^|ZXU|20190131211738|";
    public static String TEST_ITEM_ZRH = "ZRH|^~<>{}|FH|3343|MSWG|ThM|C|BAR|L|";
    public static String TEST_ITEM_ZAR =
            "ZAR{\n" +
                    "<AQUALUNG>\n" +
                    "<APP>DiverLog+</APP>\n" +
                    "<DUID>7072_3343_20190107141200_1</DUID>\n" +
                    "<TITLE></TITLE>\n" +
                    "<DIVE_DT>20190107141200</DIVE_DT>\n" +
                    "<FILE_DT>2019-01-11 20:11:35</FILE_DT>\n" +
                    "<DIVE_MODE>0</DIVE_MODE>\n" +
                    "<PDC_MODEL>i300C</PDC_MODEL>\n" +
                    "<PDC_SERIAL>3343</PDC_SERIAL>\n" +
                    "<MANUFACTURER>AQUALUNG</MANUFACTURER>\n" +
                    "<PDC_FIRMWARE>1A</PDC_FIRMWARE>\n" +
                    "<DIVER_NAME></DIVER_NAME>\n" +
                    "<LOCATION>GPS=[19.986428,-88.266621],LOCNAME=[Santa Rosa wall],CITY=[Cozumel],STATE/PROVINCE=[Quintana Roo],COUNTRY=[Mexico],AIRTEMP=25.555555555555557,SURFACETEMP=23.555555555555558,MINTEMP=24.555555555555559</LOCATION>\n" +
                    "<GEAR>GEAR_UNITS=0</GEAR>\n" +
                    "<RATING>1</RATING>\n" +
                    "<DIVESTATS>DIVENO=1,DATATYPE=8,DECO=N,VIOL=N,MODE=0,MANUALDIVE=0,EDT=005300,SI=001300,MAXDEPTH=22.860000282526016,MAXO2=0,PO2=0.00,MINTEMP=25.6</DIVESTATS>\n" +
                    "<TANK>NUMBER=1,TID=0,ON=N,CYLNAME=[Aluminium 100],CYLSIZE=100.0CU FT,WORKINGPRESSURE=3300.0PSI,STARTPRESSURE=203.000,ENDPRESSURE=60.0000,FO2=0,AVGDEPTH=14.6076,DIVETIME=53.000000,SAC=Infinity</TANK>\n" +
                    "<DIVEMEMO>eagle ray up  close</DIVEMEMO>\n" +
                    "<ARBG>0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 3, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0</ARBG>\n" +
                    "<TLBG>0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3</TLBG>\n" +
                    "<O2BG>0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0</O2BG>\n" +
                    "<ATR>0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0</ATR>\n" +
                    "<DTR>599, 216, 119, 70, 57, 59, 42, 33, 30, 31, 38, 37, 41, 42, 40, 47, 47, 42, 32, 35, 38, 29, 35, 34, 25, 27, 35, 31, 29, 31, 32, 25, 22, 19, 24, 25, 22, 18, 16, 18, 16, 16, 15, 15, 18, 18, 14, 18, 12, 13, 22, 27, 39, 54, 39, 38, 40, 37, 56, 56, 55, 35, 75, 70, 74, 69, 84, 72, 90, 89, 119, 88, 88, 81, 87, 104, 233, 232, 130, 102, 92, 49, 59, 58, 43, 23, 24, 24, 26, 36, 35, 49, 43, 42, 42, 41, 61, 112, 553, 599, 487, 599, 486, 599, 599, 599, 599</DTR>\n" +
                    "<DECOTIME>0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0</DECOTIME>\n" +
                    "<LINKED>11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11</LINKED>\n" +
                    "<VSTATUS>0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0</VSTATUS>\n" +
                    "<VARI_FLASH>0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0</VARI_FLASH>\n" +
                    "<SETPOINTS>HOUR=12,DATE=DDMMYYYY,AUDIBLE=Y,EDT=0.0,DTRON=N,MAXTLBGON=N,FREEDEPTH1ON=N,FREEDEPTH2ON=N,FREEDEPTH3ON=N,FREEEDTON=N,UNITS=1,WET=Y,FO2DEFAULT=N,DEEPSTOP=N,CONSERVE=N,ALGORITHM=0,SSTOP=1,SSTIME=5,SSDEPTH=4.0,BLDURATION=1,SAMPLE=2</SETPOINTS>\n" +
                    "</AQUALUNG>\n" +
                    "}";
    public static String TEST_ITEM_ZDH ="ZDH|1|1|I|Q30S|20190107141200|78|100.0|FO2|";
    public static String TEST_ITEM_ZDP =
            "ZDP{\n" +
                    "|0|0|1.00|\n" +
                    "|0.500000|9.14||||||29.4|||\n" +
                    "|1.000000|11.89||||||29.4|||\n" +
                    "|1.500000|14.94||||||28.9|||\n" +
                    "|2.000000|16.46||||||28.9|||\n" +
                    "|2.500000|16.15||||||28.9|||\n" +
                    "|3.000000|19.20||||||28.3|||\n" +
                    "|3.500000|21.34||||||28.3|||\n" +
                    "|4.000000|21.95||||||28.3|||\n" +
                    "|4.500000|21.64||||||27.8|||\n" +
                    "|5.000000|19.51||||||27.8|||\n" +
                    "|5.500000|19.51||||||27.8|||\n" +
                    "|6.000000|18.59||||||27.8|||\n" +
                    "|6.500000|18.59||||||27.8|||\n" +
                    "|7.000000|18.59||||||27.8|||\n" +
                    "|7.500000|17.07||||||27.2|||\n" +
                    "|8.000000|17.07||||||27.8|||\n" +
                    "|8.500000|17.98||||||27.2|||\n" +
                    "|9.000000|20.42||||||27.2|||\n" +
                    "|9.500000|19.20||||||27.2|||\n" +
                    "|10.000000|18.29||||||27.2|||\n" +
                    "|10.500000|20.42||||||27.2|||\n" +
                    "|11.000000|19.20||||||27.2|||\n" +
                    "|11.500000|19.20||||||27.2|||\n" +
                    "|12.000000|21.64||||||27.2|||\n" +
                    "|12.500000|20.42||||||27.2|||\n" +
                    "|13.000000|18.29||||||27.2|||\n" +
                    "|13.500000|19.51||||||27.2|||\n" +
                    "|14.000000|19.51||||||27.2|||\n" +
                    "|14.500000|19.20||||||27.2|||\n" +
                    "|15.000000|18.90||||||27.2|||\n" +
                    "|15.500000|20.42||||||27.2|||\n" +
                    "|16.000000|21.34||||||27.2|||\n" +
                    "|16.500000|22.25||||||27.2|||\n" +
                    "|17.000000|20.42||||||27.2|||\n" +
                    "|17.500000|19.81||||||27.2|||\n" +
                    "|18.000000|20.73||||||27.2|||\n" +
                    "|18.500000|21.95||||||27.2|||\n" +
                    "|19.000000|22.86||||||27.2|||\n" +
                    "|19.500000|21.64||||||27.2|||\n" +
                    "|20.000000|22.25||||||27.2|||\n" +
                    "|20.500000|22.25||||||27.2|||\n" +
                    "|21.000000|22.56||||||27.2|||\n" +
                    "|21.500000|21.95||||||27.2|||\n" +
                    "|22.000000|20.12||||||27.2|||\n" +
                    "|22.500000|20.42||||||27.2|||\n" +
                    "|23.000000|21.95||||||27.2|||\n" +
                    "|23.500000|19.81||||||27.2|||\n" +
                    "|24.000000|21.95||||||27.2|||\n" +
                    "|24.500000|21.34||||||27.2|||\n" +
                    "|25.000000|17.98||||||27.2|||\n" +
                    "|25.500000|15.85||||||27.2|||\n" +
                    "|26.000000|13.72||||||27.2|||\n" +
                    "|26.500000|12.80||||||27.2|||\n" +
                    "|27.000000|14.02||||||27.2|||\n" +
                    "|27.500000|13.72||||||27.2|||\n" +
                    "|28.000000|13.72||||||27.2|||\n" +
                    "|28.500000|13.41||||||27.2|||\n" +
                    "|29.000000|12.50||||||27.2|||\n" +
                    "|29.500000|12.50||||||27.2|||\n" +
                    "|30.000000|12.19||||||27.2|||\n" +
                    "|30.500000|14.02||||||27.2|||\n" +
                    "|31.000000|10.97||||||27.2|||\n" +
                    "|31.500000|11.58||||||27.2|||\n" +
                    "|32.000000|11.28||||||27.2|||\n" +
                    "|32.500000|11.58||||||27.2|||\n" +
                    "|33.000000|10.67||||||27.2|||\n" +
                    "|33.500000|10.97||||||27.2|||\n" +
                    "|34.000000|10.06||||||27.2|||\n" +
                    "|34.500000|10.36||||||27.2|||\n" +
                    "|35.000000|9.45||||||27.2|||\n" +
                    "|35.500000|10.36||||||27.2|||\n" +
                    "|36.000000|10.36||||||27.2|||\n" +
                    "|36.500000|10.36||||||27.2|||\n" +
                    "|37.000000|10.06||||||27.2|||\n" +
                    "|37.500000|9.45||||||27.2|||\n" +
                    "|38.000000|8.23||||||27.2|||\n" +
                    "|38.500000|8.23||||||27.2|||\n" +
                    "|39.000000|9.14||||||27.2|||\n" +
                    "|39.500000|9.45||||||27.2|||\n" +
                    "|40.000000|10.06||||||27.2|||\n" +
                    "|40.500000|12.50||||||27.2|||\n" +
                    "|41.000000|11.89||||||27.2|||\n" +
                    "|41.500000|11.89||||||27.2|||\n" +
                    "|42.000000|12.80||||||27.2|||\n" +
                    "|42.500000|14.94||||||27.2|||\n" +
                    "|43.000000|14.94||||||27.2|||\n" +
                    "|43.500000|14.94||||||27.2|||\n" +
                    "|44.000000|14.02||||||27.2|||\n" +
                    "|44.500000|13.11||||||27.2|||\n" +
                    "|45.000000|13.11||||||27.2|||\n" +
                    "|45.500000|12.19||||||27.2|||\n" +
                    "|46.000000|12.50||||||27.2|||\n" +
                    "|46.500000|12.19||||||27.2|||\n" +
                    "|47.000000|12.19||||||27.2|||\n" +
                    "|47.500000|12.19||||||27.2|||\n" +
                    "|48.000000|10.67||||||27.2|||\n" +
                    "|48.500000|8.84||||||27.2|||\n" +
                    "|49.000000|6.10||||||27.2|||\n" +
                    "|49.500000|5.79||||||27.2|||\n" +
                    "|50.000000|6.71||||||27.2|||\n" +
                    "|50.500000|5.49||||||27.2|||\n" +
                    "|51.000000|6.40||||||27.2|||\n" +
                    "|51.500000|5.18||||||27.2|||\n" +
                    "|52.000000|4.27||||||27.2|||\n" +
                    "|52.500000|3.96||||||27.2|||\n" +
                    "|53.000000|2.13||||||27.2|||\n" +
                    "ZDP}";
    public static String TEST_ITEM_ZDT = "ZDT|1|1|22.86|20190107150500|25.555556|142|";

    public static final String TEST_ZXU_FILE =
            TEST_ITEM_FSH + "\n" +
                    TEST_ITEM_ZRH + "\n" +
                    TEST_ITEM_ZAR + "\n" +
                    TEST_ITEM_ZDH + "\n" +
                    TEST_ITEM_ZDP + "\n" +
                    TEST_ITEM_ZDT + "\n";

    @Test
    public void zxuFileParsingTest() throws IOException {
        ZxuFile file = new DiveCloudParsingService().parseZxuFile(new StringReader(TEST_ZXU_FILE));

        assertNotNull(file);

        validateFsh(file.getFsh());
        validateZrh(file.getZrh());
        validateZar(file.getZar());
        validateZdh(file.getZdh());
        validateZdp(file.getZdp());
        validateZdt(file.getZdt());
    }

    private void validateZdp(Zdp zdp) {
        assertNotNull(zdp);
        List<ZdpRecord> samples = zdp.getSamples();
        assertNotNull(samples);
        assertEquals(106, samples.size());

        ZdpRecord sample = samples.get(0);
        assertEquals(0.5, sample.getMinutes());
        assertEquals(9.14, sample.getMeters());
        assertEquals(29.4, sample.getDegrees());

        sample = samples.get(9);
        assertEquals(5.000000, sample.getMinutes());
        assertEquals(19.51, sample.getMeters());
        assertEquals(27.8, sample.getDegrees());
    }

    private void validateZar(Zar zar) {
        assertNotNull(zar);

        assertEquals("DiverLog+", zar.getApp());
        assertEquals("7072_3343_20190107141200_1", zar.getDuid());
        assertEquals("", zar.getTitle());
        assertEquals(LocalDateTime.of(2019, 01, 07, 14, 12, 00), zar.getDiveDate());
        assertEquals(LocalDateTime.of(2019, 01, 11, 20, 11, 35), zar.getFileDate());
        assertEquals(0, zar.getDiveMode());
        assertEquals("i300C", zar.getPdcModel());
        assertEquals("3343", zar.getPdcSerial());
        assertEquals("AQUALUNG", zar.getManufacturer());
        assertEquals("1A", zar.getPdcFirmware());
        assertEquals("", zar.getDiverName());

        assertEquals(19.986428, zar.getLocation().getLatitude());
        assertEquals(-88.266621, zar.getLocation().getLongitude());
        assertEquals("Santa Rosa wall", zar.getLocation().getLocName());
        assertEquals("Cozumel", zar.getLocation().getCity());
        assertEquals("Quintana Roo", zar.getLocation().getState());
        assertEquals("Mexico", zar.getLocation().getCountry());
        assertEquals(25.555555555555557, zar.getLocation().getAirTemp());
        assertEquals(23.555555555555558, zar.getLocation().getSurfaceTemp());
        assertEquals(24.555555555555559, zar.getLocation().getMinTemp());

        assertEquals(0, zar.getGear().getGearUnits());

        assertEquals(1, zar.getRating());

        assertEquals(1, zar.getDiveStats().getDiveNo());
        assertEquals(8, zar.getDiveStats().getDataType());
        assertEquals("N", zar.getDiveStats().getDeco());
        assertEquals("N", zar.getDiveStats().getViol());
        assertEquals(0, zar.getDiveStats().getMode());
        assertEquals(0, zar.getDiveStats().getManualDive());
        assertEquals("005300", zar.getDiveStats().getEdt());
        assertEquals("001300", zar.getDiveStats().getSi());
        assertEquals(22.860000282526016, zar.getDiveStats().getMaxDepth());
        assertEquals(0.0, zar.getDiveStats().getMaxO2());
        assertEquals(0.00, zar.getDiveStats().getP02());
        assertEquals(25.6, zar.getDiveStats().getMinTemp());

        assertEquals(1, zar.getTank().getNumber());
        assertEquals(0, zar.getTank().getTid());
        assertEquals("N", zar.getTank().getOn());
        assertEquals("Aluminium 100", zar.getTank().getCylName());
        assertEquals("100.0CU FT", zar.getTank().getCylSize());
        assertEquals( "3300.0PSI", zar.getTank().getWorkingPressure());
        assertEquals(203.00, zar.getTank().getStartPressure());
        assertEquals(60.0000, zar.getTank().getEndPressure());
        assertEquals(14.6076, zar.getTank().getAvgDepth());
        assertEquals(53.000000, zar.getTank().getDiveTime());
        assertEquals("Infinity", zar.getTank().getSac());

        assertEquals("eagle ray up  close", zar.getDiveMemo());

        // add arrays validation
    }

    private void validateZdt(Zdt zdt) {
        assertNotNull(zdt);

        assertEquals(142, zdt.getBarsUsed());
        assertEquals(LocalDateTime.of(2019, 01, 07, 15, 05, 00), zdt.getEndDate());
        assertEquals(22.86, zdt.getMaxDepth());
        assertEquals(25.555556, zdt.getMinTemperature());
        assertEquals(1, zdt.getUnknown1());
        assertEquals(1, zdt.getUnknown2());
    }

    private void validateZdh(Zdh zdh) {
        assertNotNull(zdh);

        assertEquals(1, zdh.getUnknown1());
        assertEquals(1, zdh.getUnknown2());
        assertEquals("I", zdh.getUnknown3());
        assertEquals("Q30S", zdh.getSampleFrequency());
        assertEquals(LocalDateTime.of(2019, 01, 07, 14, 12, 00), zdh.getDiveDate());
        assertEquals(78.0, zdh.getUnknown4());
        assertEquals(100.0, zdh.getUnknown5());
        assertEquals("FO2", zdh.getUnknown6());
    }

    private void validateFsh(Fsh fsh) {
        assertNotNull(fsh);
        assertEquals("^~<>{}", fsh.getUnknown1());
        assertEquals("OCI201^^", fsh.getUnknown2());
        assertEquals("ZXU", fsh.getUnknown3());

        assertEquals(LocalDateTime.of(2019, 01, 31, 21, 17, 38), fsh.getUploadDate());
    }

    private void validateZrh(Zrh zrh) {
        assertNotNull(zrh);
        assertEquals("^~<>{}", zrh.getUnknown1());
        assertEquals("FH", zrh.getUnknown2());
        assertEquals("3343", zrh.getDeviceSerial());
        assertEquals("MSWG", zrh.getUnknown3());
        assertEquals("ThM", zrh.getDistanceUnit());
        assertEquals("C", zrh.getTemperatureUnit());
        assertEquals("BAR", zrh.getPressureUnit());
        assertEquals("L", zrh.getVolumeUnit());
    }
}
