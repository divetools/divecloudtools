package com.gratteburnes.divetools.divecloud.acquire.model.response;

import com.gratteburnes.divetools.divecloud.acquire.model.domain.AccountStatus;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.AccountType;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.Registered;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.RegistrationStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestRegistrationResponse {
    @Test
    public void invalidFormat_wrongHead() {
        assertThrows(IllegalArgumentException.class, () -> RegistrationResponse.from("YES VERIFIED}{FREE}{ACTIVE}"));
    }

    @Test
    public void invalidFormat_wrongTail() {
         assertThrows(IllegalArgumentException.class, () -> RegistrationResponse.from("{YES VERIFIED}{FREE}{ACTIVE"));
    }
    @Test
    public void invalidFormat_empty() {
         assertThrows(IllegalArgumentException.class, () -> RegistrationResponse.from(""));
    }
    @Test
    public void invalidFormat_blank() {
         assertThrows(IllegalArgumentException.class, () -> RegistrationResponse.from("     "));
    }
    @Test
    public void invalidFormat_null() {
         assertThrows(IllegalArgumentException.class, () -> RegistrationResponse.from(null));
    }

    @Test
    public void testRegistrationStatus_active_verified_free_active() {
        givenARegistrationBlock("{YES VERIFIED}");
        givenAnAccountTypeBlock("{FREE}");
        givenAnAccountStatusBlock("{ACTIVE}");
        whenInputIsParsed();
        thenAccountIsRegistered();
        thenAccountRegistrationIsVerified();
        thenAccountIsFree();
        thenAccountIsActive();
    }

    @Test
    public void testRegistrationStatus_active_notverified_free_active() {
        givenARegistrationBlock("{YES NOT YET VERIFIED}");
        givenAnAccountTypeBlock("{FREE}");
        givenAnAccountStatusBlock("{ACTIVE}");
        whenInputIsParsed();
        thenAccountIsRegistered();
        thenAccountRegistrationIsNotVerified();
        thenAccountIsFree();
        thenAccountIsActive();
    }

    @Test
    public void testRegistrationStatus_inactive_verified_paid_expired() {
        givenARegistrationBlock("{NO VERIFIED}");
        givenAnAccountTypeBlock("{PAID}");
        givenAnAccountStatusBlock("{EXPIRED}");
        whenInputIsParsed();
        thenAccountIsNotRegistered();
        thenAccountRegistrationIsVerified();
        thenAccountIsPaid();
        thenAccountIsExpired();
    }

    @Test
    public void testRegistrationStatus_inactive_notverified_paid_active() {
        givenARegistrationBlock("{NO NOT YET VERIFIED}");
        givenAnAccountTypeBlock("{PAID}");
        givenAnAccountStatusBlock("{ACTIVE}");
        whenInputIsParsed();
        thenAccountIsNotRegistered();
        thenAccountRegistrationIsNotVerified();
        thenAccountIsPaid();
        thenAccountIsActive();
    }

    private void thenAccountIsActive() {
        assertTrue(resp.getAccountStatus() == AccountStatus.ACTIVE);
    }
    private void thenAccountIsExpired() {
        assertTrue(resp.getAccountStatus() == AccountStatus.EXPIRED);
    }

    private void thenAccountIsFree() {
        assertTrue(resp.getAccountType() == AccountType.FREE);
    }

    private void thenAccountIsPaid() {
        assertTrue(resp.getAccountType() == AccountType.PAID);
    }

    private void thenAccountRegistrationIsVerified() {
        assertTrue(resp.getRegistrationStatus() == RegistrationStatus.VERIFIED);
    }

    private void thenAccountIsRegistered() {
        assertTrue(resp.getRegistered() == Registered.YES);
    }

    private void thenAccountRegistrationIsNotVerified() {
        assertTrue(resp.getRegistrationStatus() == RegistrationStatus.NOT_YET_VERIFIED);
    }

    private void thenAccountIsNotRegistered() {
        assertTrue(resp.getRegistered() == Registered.NO);
    }

    private void whenInputIsParsed() {
        resp = RegistrationResponse.from(String.format("%s%s%s",input[0], input[1], input[2]));
    }

    private void givenAnAccountStatusBlock(String s) {
        input[2] = s;
    }

    private void givenAnAccountTypeBlock(String s) {
        input[1] = s;
    }

    private void givenARegistrationBlock(String s) {
        input[0] = s;
    }

    private RegistrationResponse resp;
    private String[] input = new String[3];
}
