package com.gratteburnes.divetools.divecloud.acquire.model.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestInitResponse {
    @Test
    public void invalidFormat_wrongHead() {
        assertThrows(IllegalArgumentException.class, () -> InitResponse.from("YES}{SESSION=ddfdfdfd}"));
    }
//    @Test(expected = IllegalArgumentException.class)
//    public void invalidFormat_wrongTail_nl() {
//        InitResponse.from("{YES}{SESSION=dsfdf}\n");
//    }
    @Test
    public void invalidFormat_wrongTail() {
        assertThrows(IllegalArgumentException.class, () -> InitResponse.from("{YES}{SESSION=dsfdf"));
    }
    @Test
    public void invalidFormat_wrongSessionBlock() {
        assertThrows(IllegalArgumentException.class, () -> InitResponse.from("{YES}{SESSION1=dsfdf}"));
    }
    @Test
    public void invalidFormat_empty() {
        assertThrows(IllegalArgumentException.class, () -> InitResponse.from(""));
    }
    @Test
    public void invalidFormat_blank() {
        assertThrows(IllegalArgumentException.class, () -> InitResponse.from("    "));
    }
    @Test
    public void invalidFormat_null() {
        assertThrows(IllegalArgumentException.class, () -> InitResponse.from(null));
    }

    @Test
    public void testInitResponse_yes_validSession() {
        String session = "fdfdfdf";
        givenAnInitStatusBlock("{YES}");
        givenASessionBlock("{SESSION="+session+"}");
        whenInputIsParsed();
        thenInitIsSuccesful();
        thenSessionIs(session);
    }

    @Test
    public void testInitResponse_no_validSession() {
        String session = "fdfdfdf";
        givenAnInitStatusBlock("{NO}");
        givenASessionBlock("{SESSION="+session+"}");
        whenInputIsParsed();
        thenInitIsNotSuccesful();
        thenSessionIs(session);
    }

    private void thenSessionIs(String session) {
        assertEquals(session, resp.getSessionId());
    }

    private void thenInitIsSuccesful() {
        assertTrue(InitResponse.InitStatus.YES == resp.getInitStatus());
    }

    private void thenInitIsNotSuccesful() {
        assertTrue(InitResponse.InitStatus.NO == resp.getInitStatus());
    }

    private void whenInputIsParsed() {
        resp = InitResponse.from(String.format("%s%s",input[0], input[1]));
    }

    private void givenAnInitStatusBlock(String s) {
        input[0] = s;
    }

    private void givenASessionBlock(String s) {
        input[1] = s;
    }

    private InitResponse resp;
    private String[] input = new String[2];
}
