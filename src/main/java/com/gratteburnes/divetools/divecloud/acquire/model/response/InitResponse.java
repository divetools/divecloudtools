package com.gratteburnes.divetools.divecloud.acquire.model.response;

import com.gratteburnes.divetools.divecloud.acquire.model.DiveCloudConstants;

public class InitResponse extends AbstractResponse {

    public boolean isSuccessful() {
        return InitStatus.YES == getInitStatus();
    }

    public enum InitStatus {
        YES,
        NO
    }

    private InitStatus initStatus;
    private String sessionId;

    /*
    https://divecloud.net/init.py?EML=pellissp@gmail.com&PASS=ILb%23EmjEJhy&DEVICE_ID=DC_WEB&DEVICE_TYPE=1&APP_ID=DIVECLOUD&hash=0.9000384538455859
    {YES}{SESSION=c744411151160924}
     */
    public InitResponse() {

    }

    public static InitResponse from(String source) {
        String[] tokens = tokenizeSource(source);

        InitResponse resp = new InitResponse();

        parseInitStatus(resp, tokens[0]);
        parseSessionId(resp, tokens[1]);

        return resp;
    }

    private static void parseSessionId(InitResponse resp, String token) {
        validate(resp, token);
        String[] subTokens = token.split("=");
        if(! DiveCloudConstants.PARAM_SESSION.equals(subTokens[0])) {
            throw new IllegalArgumentException("Invalid session block: " + token);
        }
        resp.sessionId = subTokens[1];
    }

    private static void parseInitStatus(InitResponse resp, String token) {
        validate(resp, token);
        resp.initStatus = InitStatus.valueOf(convertToEnumName(token));
    }

    public InitStatus getInitStatus() {
        return initStatus;
    }

    public String getSessionId() {
        return sessionId;
    }

    @Override
    public String toString() {
        return "InitResponse{" +
                "initStatus=" + initStatus +
                ", sessionId='" + sessionId + '\'' +
                '}';
    }

    public InitResponse setInitStatus(InitStatus initStatus) {
        this.initStatus = initStatus;
        return this;
    }

    public InitResponse setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }
}
