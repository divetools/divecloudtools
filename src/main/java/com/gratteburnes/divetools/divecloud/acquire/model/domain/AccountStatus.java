package com.gratteburnes.divetools.divecloud.acquire.model.domain;

public enum AccountStatus {
    ACTIVE,
    EXPIRED
}