package com.gratteburnes.divetools.divecloud.acquire.model;

public class DiveCloudConstants {
    public static final String PARAM_EMAIL = "EML";
    public static final String PARAM_PASSWORD = "PASS";
    public static final String PARAM_CHECKREG = "CHECKREG";
    public static final String PARAM_DEVICEID = "DEVICE_ID";
    public static final String PARAM_DEVICETYPE = "DEVICE_TYPE";
    public static final String PARAM_APPID = "APP_ID";
    public static final String PARAM_SESSION = "SESSION";
    public static final String PARAM_LIMITS = "LIMITS";
    public static final String PARAM_FILENAME = "FILENAME";
    public static final String PARAM_HASH = "hash";
    public static final String PARAM_FOLDER = "FOLDER";
    public static final String PARAM_ITEMS = "ITEMS";


    public static final String URL_SCHEME = "https";
    public static final String URL_DOMAIN_WWW = "www.divecloud.net";
    public static final String URL_DOMAIN = "divecloud.net";

    public static final String URL_SUFFIX_CHECKREG = "checkReg.py";
    public static final String URL_SUFFIX_INIT = "init.py";
    public static final String URL_SUFFIX_WALKER = "walker.py";
    public static final String URL_SUFFIX_DOWNLOAD = "downloadFolder.py";
    /*
               ?FOLDER=" + strFolder +
              "&EML=" + strUserId +
              "&SESSION=" + strSessionId +
              "&DEVICE_ID=" + strDeviceId;
     */

    public static final String URL_SUFFIX_LOGINPAGE = "login";
    public static final String URL_SUFFIX_LOGOUTPAGE = "logout";
    public static final String URL_SUFFIX_LOGINCHECK = "logincheck";
    public static final String URL_SUFFIX_FILES = "home/filelist";
    public static final String URL_SUFFIX_DETAIL = "remote";
}
