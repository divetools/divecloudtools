package com.gratteburnes.divetools.divecloud.acquire.model.response;

import org.apache.commons.lang3.StringUtils;

public class WalkerResponse extends AbstractResponse {
    private long bytes;
    private long bytesPhotos;
    private long bytesDives;
    private int limit;
    private int percent;
    private int numFiles;
    private int numDives;
    private int numDiveLimit;
    private String type;

    public WalkerResponse() {

    }

    public static WalkerResponse from(String source) {
        if(StringUtils.isBlank(source)) {
            throw new IllegalArgumentException("Source cannot be blank");
        }
        //{Bytes=685064}{BytesPhotos=658293}{BytesDives=26771}{Limit=50}{Percent=1.37%}{NumFiles=22}{numDives=11}{numDiveLimit=10}{aType=FREE}
        String[] tokens = tokenizeSource(source);

        WalkerResponse resp = new WalkerResponse();
        for (String token : tokens) {
            String[] values = token.split("=");
            String key = values[0];
            String value = values[1];
            switch(key) {
                case "Bytes":
                    resp.bytes = Long.parseLong(value);
                    break;
                case "BytesPhotos":
                    resp.bytesPhotos = Long.parseLong(value);
                    break;
                case "BytesDives":
                    resp.bytesDives = Long.parseLong(value);
                    break;
                case "Limit":
                    resp.limit = Integer.parseInt(value);
                    break;
                case "Percent":
                    resp.percent = Integer.parseInt(value.replace("%", "").replace(".", "").replace(",", "")); // poor mans x100
                    break;
                case "NumFiles":
                    resp.numFiles = Integer.parseInt(value);
                    break;
                case "numDives": /// haha ugly
                    resp.numDives = Integer.parseInt(value);
                    break;
                case "numDiveLimit": /// haha ugly
                    resp.numDiveLimit = Double.valueOf(Double.parseDouble(value)).intValue();
                    break;
                case "aType":
                    resp.type = value;
                    break;
                default:
                    throw new RuntimeException("Unknown value token: " + key);
            }
        }
        return resp;
    }

    @Override
    public String toString() {
        return "WalkerResponse{" +
                "bytes=" + bytes +
                ", bytesPhotos=" + bytesPhotos +
                ", bytesDives=" + bytesDives +
                ", limit=" + limit +
                ", percent=" + percent +
                ", numFiles=" + numFiles +
                ", numDives=" + numDives +
                ", numDiveLimit=" + numDiveLimit +
                ", type='" + type + '\'' +
                '}';
    }

    public long getBytes() {
        return bytes;
    }

    public WalkerResponse setBytes(long bytes) {
        this.bytes = bytes;
        return this;
    }

    public long getBytesPhotos() {
        return bytesPhotos;
    }

    public WalkerResponse setBytesPhotos(long bytesPhotos) {
        this.bytesPhotos = bytesPhotos;
        return this;
    }

    public long getBytesDives() {
        return bytesDives;
    }

    public WalkerResponse setBytesDives(long bytesDives) {
        this.bytesDives = bytesDives;
        return this;
    }

    public int getLimit() {
        return limit;
    }

    public WalkerResponse setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    public int getPercent() {
        return percent;
    }

    public WalkerResponse setPercent(int percent) {
        this.percent = percent;
        return this;
    }

    public int getNumFiles() {
        return numFiles;
    }

    public WalkerResponse setNumFiles(int numFiles) {
        this.numFiles = numFiles;
        return this;
    }

    public int getNumDives() {
        return numDives;
    }

    public WalkerResponse setNumDives(int numDives) {
        this.numDives = numDives;
        return this;
    }

    public int getNumDiveLimit() {
        return numDiveLimit;
    }

    public WalkerResponse setNumDiveLimit(int numDiveLimit) {
        this.numDiveLimit = numDiveLimit;
        return this;
    }

    public String getType() {
        return type;
    }

    public WalkerResponse setType(String type) {
        this.type = type;
        return this;
    }
}
