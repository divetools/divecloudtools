package com.gratteburnes.divetools.divecloud.acquire.model.domain;

public enum RegistrationStatus {
    VERIFIED,
    NOT_YET_VERIFIED
}
