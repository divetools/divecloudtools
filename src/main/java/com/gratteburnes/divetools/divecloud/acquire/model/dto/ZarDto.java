package com.gratteburnes.divetools.divecloud.acquire.model.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName="AQUALUNG")
public class ZarDto {
    @JacksonXmlProperty(localName="APP")
    private String app;
    @JacksonXmlProperty(localName="DUID")
    private String duid;
    @JacksonXmlProperty(localName="TITLE")
    private String title;
    @JacksonXmlProperty(localName="DIVE_DT")
    private String diveDate;
    @JacksonXmlProperty(localName="FILE_DT")
    private String fileDate;
    @JacksonXmlProperty(localName="DIVE_MODE")
    private Integer diveMode;
    @JacksonXmlProperty(localName="PDC_MODEL")
    private String pdcModel;
    @JacksonXmlProperty(localName="PDC_SERIAL")
    private String pdcSerial;
    @JacksonXmlProperty(localName="MANUFACTURER")
    private String manufacturer;
    @JacksonXmlProperty(localName="PDC_FIRMWARE")
    private String pdcFirmware;
    @JacksonXmlProperty(localName="DIVER_NAME")
    private String diverName;
    @JacksonXmlProperty(localName="LOCATION")
    private String location;
    @JacksonXmlProperty(localName="GEAR")
    private String gear;
    @JacksonXmlProperty(localName="RATING")
    private Integer rating;
    @JacksonXmlProperty(localName="DIVESTATS")
    private String diveStats;
    @JacksonXmlProperty(localName="TANK")
    private String tank;
    @JacksonXmlProperty(localName="DIVEMEMO")
    private String diveMemo;
    @JacksonXmlProperty(localName="ARBG")
    private String arBg;
    @JacksonXmlProperty(localName="TLBG")
    private String tlBg;
    @JacksonXmlProperty(localName="O2BG")
    private String o2Bg;
    @JacksonXmlProperty(localName="ATR")
    private String atr;
    @JacksonXmlProperty(localName="DTR")
    private String dtr;
    @JacksonXmlProperty(localName="DECOTIME")
    private String decoTime;
    @JacksonXmlProperty(localName="LINKED")
    private String linked;
    @JacksonXmlProperty(localName="VSTATUS")
    private String vstatus;
    @JacksonXmlProperty(localName="VARI_FLASH")
    private String variFlash;
    @JacksonXmlProperty(localName="SETPOINTS")
    private String setPoints;
    @JacksonXmlProperty(localName="IMG")
    private String images;
    @JacksonXmlProperty(localName="BOOKMARK")
    private String bookmarks;
    @JacksonXmlProperty(localName="PROFILE_IMG")
    private String profileImg;
    @JacksonXmlProperty(localName="SIGNATURE")
    private String signatureFile;
    @JacksonXmlProperty(localName="V_ALARMS")
    private String alarms;

    public String getApp() {
        return app;
    }

    public ZarDto setApp(String app) {
        this.app = app;
        return this;
    }

    public String getDuid() {
        return duid;
    }

    public ZarDto setDuid(String duid) {
        this.duid = duid;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ZarDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDiveDate() {
        return diveDate;
    }

    public ZarDto setDiveDate(String diveDate) {
        this.diveDate = diveDate;
        return this;
    }

    public String getFileDate() {
        return fileDate;
    }

    public ZarDto setFileDate(String fileDate) {
        this.fileDate = fileDate;
        return this;
    }

    public Integer getDiveMode() {
        return diveMode;
    }

    public ZarDto setDiveMode(Integer diveMode) {
        this.diveMode = diveMode;
        return this;
    }

    public String getPdcModel() {
        return pdcModel;
    }

    public ZarDto setPdcModel(String pdcModel) {
        this.pdcModel = pdcModel;
        return this;
    }

    public String getPdcSerial() {
        return pdcSerial;
    }

    public ZarDto setPdcSerial(String pdcSerial) {
        this.pdcSerial = pdcSerial;
        return this;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public ZarDto setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
        return this;
    }

    public String getPdcFirmware() {
        return pdcFirmware;
    }

    public ZarDto setPdcFirmware(String pdcFirmware) {
        this.pdcFirmware = pdcFirmware;
        return this;
    }

    public String getDiverName() {
        return diverName;
    }

    public ZarDto setDiverName(String diverName) {
        this.diverName = diverName;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public ZarDto setLocation(String location) {
        this.location = location;
        return this;
    }

    public String getGear() {
        return gear;
    }

    public ZarDto setGear(String gear) {
        this.gear = gear;
        return this;
    }

    public Integer getRating() {
        return rating;
    }

    public ZarDto setRating(Integer rating) {
        this.rating = rating;
        return this;
    }

    public String getDiveStats() {
        return diveStats;
    }

    public ZarDto setDiveStats(String diveStats) {
        this.diveStats = diveStats;
        return this;
    }

    public String getTank() {
        return tank;
    }

    public ZarDto setTank(String tank) {
        this.tank = tank;
        return this;
    }

    public String getDiveMemo() {
        return diveMemo;
    }

    public ZarDto setDiveMemo(String diveMemo) {
        this.diveMemo = diveMemo;
        return this;
    }

    public String getArBg() {
        return arBg;
    }

    public ZarDto setArBg(String arBg) {
        this.arBg = arBg;
        return this;
    }

    public String getTlBg() {
        return tlBg;
    }

    public ZarDto setTlBg(String tlBg) {
        this.tlBg = tlBg;
        return this;
    }

    public String getO2Bg() {
        return o2Bg;
    }

    public ZarDto setO2Bg(String o2Bg) {
        this.o2Bg = o2Bg;
        return this;
    }

    public String getAtr() {
        return atr;
    }

    public ZarDto setAtr(String atr) {
        this.atr = atr;
        return this;
    }

    public String getDtr() {
        return dtr;
    }

    public ZarDto setDtr(String dtr) {
        this.dtr = dtr;
        return this;
    }

    public String getDecoTime() {
        return decoTime;
    }

    public ZarDto setDecoTime(String decoTime) {
        this.decoTime = decoTime;
        return this;
    }

    public String getLinked() {
        return linked;
    }

    public ZarDto setLinked(String linked) {
        this.linked = linked;
        return this;
    }

    public String getVstatus() {
        return vstatus;
    }

    public ZarDto setVstatus(String vstatus) {
        this.vstatus = vstatus;
        return this;
    }

    public String getVariFlash() {
        return variFlash;
    }

    public ZarDto setVariFlash(String variFlash) {
        this.variFlash = variFlash;
        return this;
    }

    public String getSetPoints() {
        return setPoints;
    }

    public ZarDto setSetPoints(String setPoints) {
        this.setPoints = setPoints;
        return this;
    }

    public String getImages() {
        return images;
    }

    public ZarDto setImages(String images) {
        this.images = images;
        return this;
    }

    public String getBookmarks() {
        return bookmarks;
    }

    public ZarDto setBookmarks(String bookmarks) {
        this.bookmarks = bookmarks;
        return this;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public ZarDto setProfileImg(String profileImg) {
        this.profileImg = profileImg;
        return this;
    }

    public String getSignatureFile() {
        return signatureFile;
    }

    public ZarDto setSignatureFile(String signatureFile) {
        this.signatureFile = signatureFile;
        return this;
    }

    public String getAlarms() {
        return alarms;
    }

    public ZarDto setAlarms(String alarms) {
        this.alarms = alarms;
        return this;
    }
}
