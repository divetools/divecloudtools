package com.gratteburnes.divetools.divecloud.acquire.model.response;

import com.gratteburnes.divetools.divecloud.acquire.model.domain.AccountStatus;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.AccountType;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.Registered;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.RegistrationStatus;

public class RegistrationResponse extends AbstractResponse {
    private Registered registered;
    private RegistrationStatus registrationStatus;
    private AccountType accountType;
    private AccountStatus accountStatus;

    /*
    https://divecloud.net/checkReg.py?EML=pellissp@gmail.com&CHECKREG=1&hash=0.912293553718571
    {YES VERIFIED}{FREE}{ACTIVE}
     */
    public RegistrationResponse() {

    }

    public static RegistrationResponse from(String source) {
        String[] tokens = tokenizeSource(source);

        RegistrationResponse resp = new RegistrationResponse();

        parseRegistration(resp, tokens[0]);
        parseAccountType(resp, tokens[1]);
        parseAccountStatus(resp, tokens[2]);

        return resp;
    }

    private static void parseAccountStatus(RegistrationResponse resp, String token) {
        validate(resp, token);
        resp.accountStatus = AccountStatus.valueOf(convertToEnumName(token));
    }

    private static void parseAccountType(RegistrationResponse resp, String token) {
        validate(resp, token);
        resp.accountType = AccountType.valueOf(convertToEnumName(token));
    }

    private static void parseRegistration(RegistrationResponse resp, String token) {
        validate(resp, token);

        int firstSpace = token.indexOf(" ");

        resp.registered = Registered.valueOf(convertToEnumName(token.substring(0, firstSpace)));
        resp.registrationStatus = RegistrationStatus.valueOf(convertToEnumName(token.substring(firstSpace + 1)));
    }

    public Registered getRegistered() {
        return registered;
    }

    public RegistrationStatus getRegistrationStatus() {
        return registrationStatus;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public boolean isRegistrationVerified() {
        return RegistrationStatus.VERIFIED == getRegistrationStatus();
    }

    public boolean isRegistered() {
        return Registered.YES == getRegistered();
    }

    public boolean isActive() {
        return AccountStatus.ACTIVE == getAccountStatus();
    }

    @Override
    public String toString() {
        return "RegistrationResponse{" +
                "registered=" + registered +
                ", registrationStatus=" + registrationStatus +
                ", accountType=" + accountType +
                ", accountStatus=" + accountStatus +
                '}';
    }

    public RegistrationResponse setRegistered(Registered registered) {
        this.registered = registered;
        return this;
    }

    public RegistrationResponse setRegistrationStatus(RegistrationStatus registrationStatus) {
        this.registrationStatus = registrationStatus;
        return this;
    }

    public RegistrationResponse setAccountType(AccountType accountType) {
        this.accountType = accountType;
        return this;
    }

    public RegistrationResponse setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
        return this;
    }
}
