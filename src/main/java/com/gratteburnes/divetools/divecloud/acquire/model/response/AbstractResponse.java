package com.gratteburnes.divetools.divecloud.acquire.model.response;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

public abstract class AbstractResponse {
    protected static final String TOKEN_START = "{";
    protected static final String TOKEN_END = "}";


    protected static String convertToEnumName(String string) {
        return string == null ? "NULL": string.trim().replace(" ", "_");
    }

    protected static String convertFromEnumName(String string) {
        return string == null ? "": string.replace("_", " ");
    }

    protected static void validate(AbstractResponse resp, String token) {
        if(resp == null) {
            throw new IllegalArgumentException("Response cannot be null");
        }
        if(StringUtils.isBlank(token)) {
            throw new IllegalArgumentException("Token cannot be blank");
        }
    }

    protected static String[] tokenizeSource(String source) {
        if(StringUtils.isBlank(source)) {
            throw new IllegalArgumentException("Source cannot be blank");
        }

        // payload may come newline terminated
        String work = source.replaceAll("\\n", "");

        if(!work.startsWith(TOKEN_START) || !work.endsWith(TOKEN_END)) {
            throw new IllegalArgumentException("Invalid data format: '" + work+"'");
        }

        work = work.replaceAll(Pattern.quote(TOKEN_START), "");
        return work.split(Pattern.quote(TOKEN_END));
    }

}
