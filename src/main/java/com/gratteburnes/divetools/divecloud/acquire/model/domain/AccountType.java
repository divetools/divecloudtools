package com.gratteburnes.divetools.divecloud.acquire.model.domain;

public enum AccountType {
    FREE,
    PAID
}