package com.gratteburnes.divetools.divecloud.acquire.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class FilesResponse extends AbstractResponse {

    @JsonProperty("chart")
    private Map<String, List<String[]>> chart;
    @JsonProperty("count")
    private int count;
    @JsonProperty("err")
    private Map<String, String> errors;

    public FilesResponse() {

    }

    public static FilesResponse from(String from) throws IOException {
        return new ObjectMapper().readValue(from, FilesResponse.class);
    }

    public Map<String, List<String[]>> getChart() {
        return chart;
    }

    public FilesResponse setChart(Map<String, List<String[]>> chart) {
        this.chart = chart;
        return this;
    }

    public int getCount() {
        return count;
    }

    public FilesResponse setCount(int count) {
        this.count = count;
        return this;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public FilesResponse setErrors(Map<String, String> errors) {
        this.errors = errors;
        return this;
    }

    @Override
    public String toString() {
        return "FilesResponse{" +
                "chart=" + chart +
                ", count=" + count +
                ", errors=" + errors +
                '}';
    }
}
