package com.gratteburnes.divetools.divecloud.acquire.service;

import com.gratteburnes.divetools.divecloud.acquire.model.DiveCloudConstants;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.AccountStatus;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.AccountType;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.Registered;
import com.gratteburnes.divetools.divecloud.acquire.model.domain.RegistrationStatus;
import com.gratteburnes.divetools.divecloud.acquire.model.response.FilesResponse;
import com.gratteburnes.divetools.divecloud.acquire.model.response.InitResponse;
import com.gratteburnes.divetools.divecloud.acquire.model.response.RegistrationResponse;
import com.gratteburnes.divetools.divecloud.acquire.model.response.WalkerResponse;
import com.gratteburnes.divetools.divecloud.tools.ResponseTools;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpCookie;
import java.net.URI;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.gratteburnes.divetools.divecloud.acquire.model.DiveCloudConstants.*;
import static com.gratteburnes.divetools.divecloud.tools.ExceptionTools.illegal;
import static com.gratteburnes.divetools.divecloud.tools.ExceptionTools.runtime;

public class DiveCloudFetchingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiveCloudFetchingService.class);

    private static String DEVICE_ID = "DC_WEB";
    private static int DEVICE_TYPE = 1;
    private static String APP_ID = "DIVECLOUD";

    private Random random = new Random(System.currentTimeMillis());

    private RestTemplate restTemplate = new RestTemplate();

    public Map<String, byte[]> getDiveFilesFromArchive(byte[] archiveBytes) throws IOException {
        if(archiveBytes == null || archiveBytes.length == 0) {
            throw new IllegalArgumentException("Archive bytes cannot be empty or null");
        }
        return readFromZipFile(archiveBytes);
    }

    public Map<String, byte[]> getDiveFilesFromDiveCloud(String email, String password) throws IOException {
        if(StringUtils.isBlank(email)) {
            throw new IllegalArgumentException("Email cannot be blank");
        }
        if(StringUtils.isBlank(password)) {
            throw new IllegalArgumentException("Password cannot be blank");
        }

        HttpCookie symfonyCookie = fetchLoginCookie();
        try {
            RegistrationResponse reg = checkReg(email, symfonyCookie);

            continueIfRegistrationOk(reg);

            InitResponse init = init(email, password, DEVICE_ID, DEVICE_TYPE, APP_ID, symfonyCookie);

            continueIfInitOK(init);

            String session = init.getSessionId();

            LOGGER.debug("SessionId: " + session);

            symfonyCookie = loginCheck(email, password, DEVICE_ID, session, symfonyCookie);

            LOGGER.debug("Symfony cookie: " + symfonyCookie.toString());

            FilesResponse files = files(email, DEVICE_ID, session, symfonyCookie);

            //WalkerResponse walk = walk(email, DEVICE_ID, session, symfonyCookie);

            LOGGER.debug("Downloaded files" + files);

            String items = files.getChart().keySet().stream().reduce("", (s1, s2) -> ("".equals(s1) ? "" : s1 + "|") + s2.replaceAll(".zxu", ""));

            Map<String, byte[]> contents = downloadAll(items, email, DEVICE_ID, session, symfonyCookie);

            return contents;
         } finally {
            logout(symfonyCookie);
        }
    }


    private Map<String, byte[]> downloadAll(String files, String email, String deviceId, String session, HttpCookie symfonyCookie) throws IOException {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(URL_SCHEME)
                .host(URL_DOMAIN)
                .path(URL_SUFFIX_DOWNLOAD)
                .build();


        MultiValueMap<String, Object> map= new LinkedMultiValueMap<>();
        map.add(PARAM_EMAIL, email);
        map.add(PARAM_FOLDER, "ALL");
        map.add(PARAM_ITEMS, files);
        map.add(PARAM_DEVICEID, deviceId);
        map.add(PARAM_SESSION, session);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, buildHeaders(symfonyCookie));

        ResponseEntity<byte[]> resp = restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, request, byte[].class);

        validateResponse(resp);

        return readFromZipFile(resp.getBody());
    }

    private Map<String, byte[]> readFromZipFile(byte[] body) throws IOException {
        Map<String, byte[]> diveFiles = new LinkedHashMap<>();

        byte[] buffer = new byte[2048];
        try (ByteArrayInputStream bis = new ByteArrayInputStream(Objects.requireNonNull(body));
             ZipInputStream zis = new ZipInputStream(bis)) {

            ZipEntry zipEntry;

            while ((zipEntry = zis.getNextEntry()) != null) {
                try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {

                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        bos.write(buffer, 0, len);
                    }
                    bos.flush();
                    byte[] content = bos.toByteArray();
                    diveFiles.put(zipEntry.getName(), content);
                }
            }
        }

        return diveFiles;
    }

    private String downloadResponse(String fileName, String email, String deviceId, String session, HttpCookie symfonyCookie) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(URL_SCHEME)
                .host(URL_DOMAIN_WWW)
                .path(URL_SUFFIX_DOWNLOAD)
                .build();


        MultiValueMap<String, Object> map= new LinkedMultiValueMap<>();
        map.add(PARAM_EMAIL, email);
        map.add(PARAM_FILENAME, fileName);
        map.add(PARAM_DEVICEID, deviceId);
        map.add(PARAM_SESSION, session);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, buildHeaders(symfonyCookie));

        ResponseEntity<String> resp = restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, request, String.class);

        validateResponse(resp);

        return resp.getBody();
    }

    private void logout(HttpCookie symfonyCookie) {
        HttpEntity<MultiValueMap<String, Object>> request = buildRequest(
                buildHeaders(symfonyCookie),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

        ResponseEntity<String> resp = restTemplate.exchange(buildUri(DiveCloudConstants.URL_SUFFIX_LOGOUTPAGE), HttpMethod.GET, request, String.class);

        validateResponse(resp);
    }

    private HttpEntity<MultiValueMap<String, Object>> buildRequest(HttpHeaders headers,
                                                                   String email,
                                                                   String password,
                                                                   String deviceId,
                                                                   String session,
                                                                   String limits,
                                                                   Float hash,
                                                                   String folder,
                                                                   String files) {
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        addIfNotNull(email, DiveCloudConstants.PARAM_EMAIL, map);
        addIfNotNull(password, DiveCloudConstants.PARAM_PASSWORD, map);
        addIfNotNull(deviceId, DiveCloudConstants.PARAM_DEVICEID, map);
        addIfNotNull(session, DiveCloudConstants.PARAM_SESSION, map);
        addIfNotNull(limits, DiveCloudConstants.PARAM_LIMITS, map);
        addIfNotNull(hash, DiveCloudConstants.PARAM_HASH, map);
        addIfNotNull(folder, DiveCloudConstants.PARAM_FOLDER, map);
        addIfNotNull(files, DiveCloudConstants.PARAM_ITEMS, map);

        return new HttpEntity<>(map, headers);
    }

    private void addIfNotNull(Object o, String name, MultiValueMap<String, Object> map) {
        if(o != null) {
            map.add(name, o);
        }
    }

    private URI buildUri(String path) {
        return UriComponentsBuilder.newInstance()
                .scheme(DiveCloudConstants.URL_SCHEME)
                .host(DiveCloudConstants.URL_DOMAIN_WWW)
                .path(path)
                .build()
                .toUri();
    }

    private HttpCookie loginCheck(String email, String password, String deviceId, String session, HttpCookie symfonyCookie) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(URL_SCHEME)
                .host(URL_DOMAIN_WWW)
                .path(URL_SUFFIX_LOGINCHECK)
                .build();


        MultiValueMap<String, Object> map= new LinkedMultiValueMap<>();
        map.add(PARAM_EMAIL, email);
        map.add(PARAM_PASSWORD, password);
        map.add(PARAM_DEVICEID, deviceId);
        map.add(PARAM_SESSION, session);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, buildHeaders(symfonyCookie));

        ResponseEntity<String> resp = restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, request, String.class);

        validateResponse(resp);

        return getSymfonyCookie(resp);
    }

    private HttpCookie fetchLoginCookie() {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host(URL_DOMAIN_WWW)
                .path(URL_SUFFIX_LOGINPAGE)
                .build();

        ResponseEntity<String> resp = restTemplate.getForEntity(uriComponents.toUriString(), String.class);

        validateResponse(resp);

        return getSymfonyCookie(resp);
    }

    private HttpCookie getSymfonyCookie(ResponseEntity<String> resp) {
        List<String> cookies = resp.getHeaders().getValuesAsList(HttpHeaders.SET_COOKIE);
        if(cookies == null || cookies.isEmpty()) {
            runtime("No Set-Cookie header found");
        }
        String symfonyCookieString = cookies.get(0);
        if(! symfonyCookieString.startsWith("symfony")) {
            runtime("No symfony cookie found");
        }
        List<HttpCookie> parsedCookies = HttpCookie.parse(symfonyCookieString);
        return parsedCookies.get(0);
    }

    private FilesResponse files(String email, String deviceId, String sessionId, HttpCookie symfonyCookie) throws IOException {
        validateEmail(email);
        validateSessionId(sessionId);

        MultiValueMap<String, Object> params= new LinkedMultiValueMap<>();
        params.add(PARAM_HASH, generateHash());


        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(URL_SCHEME)
                .host(URL_DOMAIN_WWW)
                .path(URL_SUFFIX_FILES)
                .query(queryString(PARAM_HASH))
                .buildAndExpand(params);

        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(null, buildHeaders(symfonyCookie));

        ResponseEntity<String> resp = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, request, String.class);

        validateResponse(resp);

        return FilesResponse.from(resp.getBody());
    }

    private WalkerResponse walk(String email, String deviceId, String sessionId, HttpCookie symfonyCookie) {
        validateEmail(email);
        validateSessionId(sessionId);

        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(URL_SCHEME)
                .host(URL_DOMAIN_WWW)
                .path(URL_SUFFIX_WALKER)
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, Object> map= new LinkedMultiValueMap<>();
        map.add(PARAM_EMAIL, email);
        map.add(PARAM_LIMITS, "Y");
        map.add(PARAM_DEVICEID, deviceId);
        map.add(PARAM_SESSION, sessionId);
        map.add(PARAM_HASH, generateHash());

        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> resp = restTemplate.postForEntity(uriComponents.toUri(), request, String.class);

        validateResponse(resp);

        return parseWalkerResponse(resp.getBody());
    }

    public WalkerResponse parseWalkerResponse(String source) {
        if(StringUtils.isBlank(source)) {
            illegal("Source cannot be blank");
        }
        //{Bytes=685064}{BytesPhotos=658293}{BytesDives=26771}{Limit=50}{Percent=1.37%}{NumFiles=22}{numDives=11}{numDiveLimit=10}{aType=FREE}
        String[] tokens = ResponseTools.tokenizeSource(source);

        WalkerResponse resp = new WalkerResponse();
        for (String token : tokens) {
            String[] values = token.split("=");
            String key = values[0];
            String value = values[1];
            switch(key) {
                case "Bytes":
                    resp.setBytes(Long.parseLong(value));
                    break;
                case "BytesPhotos":
                    resp.setBytesPhotos(Long.parseLong(value));
                    break;
                case "BytesDives":
                    resp.setBytesDives(Long.parseLong(value));
                    break;
                case "Limit":
                    resp.setLimit(Integer.parseInt(value));
                    break;
                case "Percent":
                    resp.setPercent(Integer.parseInt(value.replace("%", "").replace(".", "").replace(",", ""))); // poor mans x10)0
                    break;
                case "NumFiles":
                    resp.setNumFiles(Integer.parseInt(value));
                    break;
                case "numDives": /// haha ugly
                    resp.setNumDives(Integer.parseInt(value));
                    break;
                case "numDiveLimit": /// haha ugly
                    resp.setNumDiveLimit(Double.valueOf(Double.parseDouble(value)).intValue());
                    break;
                case "aType":
                    resp.setType(value);
                    break;
                default:
                    runtime("Unknown value token: " + key);
            }
        }
        return resp;
    }

    private void continueIfInitOK(InitResponse init) {
        if(init == null) {
            illegal("Init response was null");
        }
        if(! init.isSuccessful()) {
            runtime("Initialization failed");
        }
        validateSessionId(init.getSessionId());
    }

    private void continueIfRegistrationOk(RegistrationResponse reg) {
        if(reg == null) {
            illegal("Registration response was null");
        }
        if(Registered.YES != reg.getRegistered()) {
            runtime("Account is not registered");
        }
        if(RegistrationStatus.VERIFIED != reg.getRegistrationStatus()){
            runtime("Registration is not verified");
        }
        if(RegistrationStatus.VERIFIED != reg.getRegistrationStatus()) {
            runtime("Account is not active");
        }
    }

    private InitResponse init(String email, String password, String deviceId, int deviceType, String appId, HttpCookie symfonyCookie) {
        validateEmail(email);

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_EMAIL, email);
        params.put(PARAM_PASSWORD, password);
        params.put(PARAM_APPID, appId);
        params.put(PARAM_DEVICEID, deviceId);
        params.put(PARAM_DEVICETYPE, deviceType);
        params.put(PARAM_HASH, generateHash());

        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(URL_SCHEME)
                .host(URL_DOMAIN_WWW)
                .path(URL_SUFFIX_INIT)
                .query(queryString(PARAM_EMAIL))
                .query(queryString(PARAM_PASSWORD))
                .query(queryString(PARAM_DEVICEID))
                .query(queryString(PARAM_DEVICETYPE))
                .query(queryString(PARAM_APPID))
                .query(queryString(PARAM_HASH))
                .buildAndExpand(params);

        MultiValueMap<String, String> headers = buildHeaders(symfonyCookie);
        headers.add(HttpHeaders.CONTENT_LENGTH, "0");
        HttpEntity entity = new HttpEntity(null, headers);

        ResponseEntity<String> resp = restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, entity, String.class);

        validateResponse(resp);

        return parseInitResponse(resp.getBody());
    }

    public InitResponse parseInitResponse(String source) {
        String[] tokens = ResponseTools.tokenizeSource(source);

        InitResponse resp = new InitResponse();

        parseInitStatus(resp, tokens[0]);
        parseSessionId(resp, tokens[1]);

        return resp;
    }

    private void parseSessionId(InitResponse resp, String token) {
        ResponseTools.validate(resp, token);
        String[] subTokens = token.split("=");
        if(! PARAM_SESSION.equals(subTokens[0])) {
            illegal("Invalid session block: " + token);
        }
        resp.setSessionId(subTokens[1]);
    }

    private void parseInitStatus(InitResponse resp, String token) {
        ResponseTools.validate(resp, token);
        resp.setInitStatus(InitResponse.InitStatus.valueOf(ResponseTools.convertResponseToEnumName(token)));
    }

    private RegistrationResponse checkReg(String email, HttpCookie symfonyCookie) {
        validateEmail(email);

        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_EMAIL, email);
        params.put(PARAM_CHECKREG, 1);
        params.put(PARAM_HASH, generateHash());

        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(URL_SCHEME)
                .host(URL_DOMAIN_WWW)
                .path(URL_SUFFIX_CHECKREG)
                .query(queryString(PARAM_EMAIL))
                .query(queryString(PARAM_CHECKREG))
                .query(queryString(PARAM_HASH))
                .buildAndExpand(params);

        HttpEntity entity = new HttpEntity("", buildHeaders(symfonyCookie));

        ResponseEntity<String> resp = restTemplate.exchange(uriComponents.toUri(), HttpMethod.GET, entity, String.class);

        validateResponse(resp);

        return parseRegistrationResponse(resp.getBody());
    }

    public RegistrationResponse parseRegistrationResponse(String source) {
        String[] tokens = ResponseTools.tokenizeSource(source);

        RegistrationResponse resp = new RegistrationResponse();

        parseRegistration(resp, tokens[0]);
        parseAccountType(resp, tokens[1]);
        parseAccountStatus(resp, tokens[2]);

        return resp;
    }

    private void parseAccountStatus(RegistrationResponse resp, String token) {
        ResponseTools.validate(resp, token);
        resp.setAccountStatus(AccountStatus.valueOf(ResponseTools.convertResponseToEnumName(token)));
    }

    private void parseAccountType(RegistrationResponse resp, String token) {
        ResponseTools.validate(resp, token);
        resp.setAccountType(AccountType.valueOf(ResponseTools.convertResponseToEnumName(token)));
    }

    private void parseRegistration(RegistrationResponse resp, String token) {
        ResponseTools.validate(resp, token);

        int firstSpace = token.indexOf(" ");

        resp.setRegistered(Registered.valueOf(ResponseTools.convertResponseToEnumName(token.substring(0, firstSpace))));
        resp.setRegistrationStatus(RegistrationStatus.valueOf(ResponseTools.convertResponseToEnumName(token.substring(firstSpace + 1))));
    }

    private HttpHeaders buildHeaders(HttpCookie symfonyCookie) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.COOKIE, symfonyCookie.toString());
        return headers;
    }

    private void validateResponse(ResponseEntity<?> resp) {
        if(resp.getStatusCode() != HttpStatus.OK) {
            throw new RuntimeException(resp.getStatusCode().toString() + "\n" + resp.getBody());
        }
    }

    private String queryString(String paramName) {
        return String.format("%s={%s}", paramName, paramName);
    }

    private float generateHash() {
        return random.nextFloat();
    }

    private void validateEmail(String email) {
        if(StringUtils.isBlank(email)) {
            illegal("Email cannot be blank");
        }
    }

    private void validateSessionId(String sessionId) {
        if(StringUtils.isBlank(sessionId)) {
            runtime("Session id was blank");
        }
    }
}
