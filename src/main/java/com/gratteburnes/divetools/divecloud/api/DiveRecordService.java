package com.gratteburnes.divetools.divecloud.api;

import com.gratteburnes.divetools.divecloud.acquire.service.DiveCloudFetchingService;
import com.gratteburnes.divetools.divecloud.parse.model.ZxuFile;
import com.gratteburnes.divetools.divecloud.parse.service.DiveCloudParsingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.io.*;
import java.nio.file.Files;
import java.util.*;

public class DiveRecordService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiveRecordService.class);

    private boolean dumpImages = false;

    private DiveCloudFetchingService diveCloudFetchingService = new DiveCloudFetchingService();
    private DiveCloudParsingService diveCloudParsingService = new DiveCloudParsingService();

    public Map<String, ZxuFile> getDiveDataFromArchive(byte[] archive) throws IOException {
        Map<String, byte[]> contents = diveCloudFetchingService.getDiveFilesFromArchive(archive);
        LOGGER.info("Got {} files from archive", contents.size());
        return processContents(contents);
    }

    public Map<String, ZxuFile> getDiveFilesFromDiveCloud(String userName, String password) throws IOException {
        Map<String, byte[]> contents = diveCloudFetchingService.getDiveFilesFromDiveCloud(userName, password);
        LOGGER.info("Got {} files from divecloud", contents.size());
        return processContents(contents);
    }

    private Map<String, ZxuFile> processContents(Map<String, byte[]> contents) {
        Map<String, ZxuFile> zxuFiles = new LinkedHashMap<>();
        Map<String, Map<String, byte[]>> imageMap = new HashMap<>();

        LOGGER.info("Parsing ZXU files...");
        contents.entrySet().forEach(entry -> {
            String key = entry.getKey();
            String[] splitKey = splitKey(key);
            byte[] fileContents = entry.getValue();
            try {
                if(isImageFile(key)) {
                    Map<String, byte[]> images = imageMap.get(splitKey);
                    if (images == null) {
                        images = new LinkedHashMap<>();
                        imageMap.put(splitKey[0], images);
                    }

                    images.put(splitKey[1], fileContents);
                    if(dumpImages) {
                        ImageIO.write(ImageIO.read(new ByteArrayInputStream(fileContents)), "jpg", new File(splitKey[1]));
                    }
                } else if(isZxuFile(key)){
                    ZxuFile zxuFile = diveCloudParsingService.parseZxuFile(new BufferedReader(new StringReader(new String(fileContents))));
                    zxuFiles.put(splitKey[0], zxuFile);
                }
            } catch (IOException e) {
                LOGGER.error("While parsing zxu file: " + fileContents, e);
            }
        });

        LOGGER.info("Injecting images");
        // put images in the right records
        zxuFiles.entrySet().forEach(e ->
        {
            String key = e.getKey();
            ZxuFile zxuFile = e.getValue();
            Map<String, byte[]> images = imageMap.get(key);
            Map<String, byte[]> filtered = filterImages(images, new HashSet<>(zxuFile.getZar().getImageNames()));
            zxuFile.setImages(filtered);
            if (images != null) {
                zxuFile.setSignature(images.get(zxuFile.getZar().getSignatureImageName()));
            }
        });
        LOGGER.info("Processed {} ZXU files", zxuFiles.size());
        return zxuFiles;
    }

    private Map<String, byte[]> filterImages(Map<String, byte[]> images, Set<String> imageNames) {
        Map<String, byte[]> result = new HashMap<>();

        if (images != null && imageNames != null) {
            for (String imgName : images.keySet()) {
                if (imageNames.contains(imgName)) {
                    byte[] bytes = images.get(imgName);
                    result.put(imgName, bytes);
                } else {
                    LOGGER.debug("Filtering out image {}", imgName);
                }
            }
        }

        return result;
    }

    private String[] splitKey(String key) {
        return key.split("/");
    }

    private boolean isZxuFile(String key) {
        return key != null && key.endsWith(".zxu");
    }

    private boolean isImageFile(String key) {
        return key != null && (
                key.endsWith(".jpeg") ||
                        key.endsWith(".jpg") ||
                        key.endsWith(".png")
        );
    }

    public static void main(String[] args) throws IOException {
        //List<ZxuFile> data = new DiveRecordService().getDiveFilesFromDiveCloud(args[0], args[1]);
        Map<String, ZxuFile> data = new DiveRecordService().getDiveDataFromArchive(Files.readAllBytes(new File("G:\\My Drive\\Perso\\dives\\divecloud_20200911221536.zip").toPath()));
        System.out.println("Found " + data.size() + " dives");
    }
}
