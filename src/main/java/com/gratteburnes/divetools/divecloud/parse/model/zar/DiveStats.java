package com.gratteburnes.divetools.divecloud.parse.model.zar;

public class DiveStats {
    private Integer diveNo;
    private Integer dataType;
    private String deco;
    private String viol;
    private Integer mode;
    private Integer manualDive;
    private String edt;
    private String si;
    private Double maxDepth;
    private Double maxO2;
    private Double p02;
    private Double minTemp;

    public DiveStats() {

    }

    public Integer getDiveNo() {
        return diveNo;
    }

    public DiveStats setDiveNo(Integer diveNo) {
        this.diveNo = diveNo;
        return this;
    }

    public Integer getDataType() {
        return dataType;
    }

    public DiveStats setDataType(Integer dataType) {
        this.dataType = dataType;
        return this;
    }

    public String getDeco() {
        return deco;
    }

    public DiveStats setDeco(String deco) {
        this.deco = deco;
        return this;
    }

    public String getViol() {
        return viol;
    }

    public DiveStats setViol(String viol) {
        this.viol = viol;
        return this;
    }

    public Integer getMode() {
        return mode;
    }

    public DiveStats setMode(Integer mode) {
        this.mode = mode;
        return this;
    }

    public Integer getManualDive() {
        return manualDive;
    }

    public DiveStats setManualDive(Integer manualDive) {
        this.manualDive = manualDive;
        return this;
    }

    public String getEdt() {
        return edt;
    }

    public DiveStats setEdt(String edt) {
        this.edt = edt;
        return this;
    }

    public String getSi() {
        return si;
    }

    public DiveStats setSi(String si) {
        this.si = si;
        return this;
    }

    public Double getMaxDepth() {
        return maxDepth;
    }

    public DiveStats setMaxDepth(Double maxDepth) {
        this.maxDepth = maxDepth;
        return this;
    }

    public Double getMaxO2() {
        return maxO2;
    }

    public DiveStats setMaxO2(Double maxO2) {
        this.maxO2 = maxO2;
        return this;
    }

    public Double getP02() {
        return p02;
    }

    public DiveStats setP02(Double p02) {
        this.p02 = p02;
        return this;
    }

    public Double getMinTemp() {
        return minTemp;
    }

    public DiveStats setMinTemp(Double minTemp) {
        this.minTemp = minTemp;
        return this;
    }
}
