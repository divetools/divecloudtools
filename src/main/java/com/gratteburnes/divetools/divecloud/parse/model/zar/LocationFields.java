package com.gratteburnes.divetools.divecloud.parse.model.zar;

public enum LocationFields {
    GPS,
    LOCNAME,
    CITY,
    STATE_PROVINCE,
    COUNTRY,
    AIRTEMP,
    SURFACETEMP,
    MINTEMP,
    VISIBILITY;
}
