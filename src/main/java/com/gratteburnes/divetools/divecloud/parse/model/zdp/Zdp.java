package com.gratteburnes.divetools.divecloud.parse.model.zdp;

import java.util.LinkedList;
import java.util.List;

public class Zdp { // dive profile
    public static final String TOKEN_HEADER = "ZDP";

    private List<ZdpRecord> samples;

    public Zdp() {
        samples = new LinkedList<>();
    }

    public List<ZdpRecord> getSamples() {
        return samples;
    }

    public Zdp setSamples(List<ZdpRecord> samples) {
        this.samples = samples;
        return this;
    }
}
