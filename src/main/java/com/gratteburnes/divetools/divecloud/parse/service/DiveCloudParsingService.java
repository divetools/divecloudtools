package com.gratteburnes.divetools.divecloud.parse.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gratteburnes.divetools.divecloud.acquire.model.dto.ZarDto;
import com.gratteburnes.divetools.divecloud.parse.model.ZxuFile;
import com.gratteburnes.divetools.divecloud.parse.model.fsh.Fsh;
import com.gratteburnes.divetools.divecloud.parse.model.zar.*;
import com.gratteburnes.divetools.divecloud.parse.model.zdh.Zdh;
import com.gratteburnes.divetools.divecloud.parse.model.zdp.Zdp;
import com.gratteburnes.divetools.divecloud.parse.model.zdp.ZdpRecord;
import com.gratteburnes.divetools.divecloud.parse.model.zdt.Zdt;
import com.gratteburnes.divetools.divecloud.parse.model.zrh.Zrh;
import com.gratteburnes.divetools.divecloud.tools.ValidationTools;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.gratteburnes.divetools.divecloud.tools.ExceptionTools.illegal;
import static com.gratteburnes.divetools.divecloud.tools.ExceptionTools.runtime;

public class DiveCloudParsingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiveCloudParsingService.class);

    public static final String TOKEN_SEPARATOR = "|";

    public static final String ZDP_START = "ZDP{";
    public static final String ZDP_END = "ZDP}";

    public static final String ZAR_START = "ZAR{";
    public static final String ZAR_END = "}";

    public ZxuFile parseZxuFile(String path) throws IOException {
        ValidationTools.validateNotBlank(path);

        try(Reader r = new InputStreamReader(Objects.requireNonNull(ZxuFile.class.getClassLoader().getResourceAsStream(path)))){
            return parseZxuFile(r);
        }
    }

    public ZxuFile parseZxuFile(Reader r) throws IOException {
        try(BufferedReader br = new BufferedReader(r)) {
            return parseZxuFile(br);
        }
    }

    public ZxuFile parseZxuFile(BufferedReader br) throws IOException {
        ZxuFile file = new ZxuFile();
        String firstLine = br.readLine();


        file.setFsh(parseFsh(firstLine));

        file.setZrh(parseZrh(br.readLine()));

        // build zar lines
        StringBuilder sb = new StringBuilder();
        String line;
        while(!"}".equals(line = br.readLine())) {
            sb.append(line);
        }
        sb.append(line);
        file.setZar(parseZar(sb.toString()));

        file.setZdh(parseZdh(br.readLine()));

        // build zdp lines
        sb = new StringBuilder();
        while(!ZDP_END.equals(line = br.readLine())) {
            sb.append(line);
            sb.append("\n");
        }
        sb.append(line);
        sb.append("\n");
        file.setZdp(parseZdp(sb.toString()));

        file.setZdt(parseZdt(br.readLine()));

        return file;
    }

    public Zdt parseZdt(String source) {
        ValidationTools.validateNotBlank(source);

        String[] tokens = source.split(Pattern.quote(TOKEN_SEPARATOR));

        ValidationTools.validateHeader(tokens[0], Zdt.TOKEN_HEADER);

        Zdt zdt = new Zdt();
        try {
            zdt.setUnknown1(Integer.parseInt(tokens[1]));
        } catch (NumberFormatException e) {
            LOGGER.warn("While parsing unknown1 from '{}' : {}", source, e.getMessage());
        }
        try {
            zdt.setUnknown2(Integer.parseInt(tokens[2]));
        } catch (NumberFormatException e) {
            LOGGER.warn("While parsing unknown2 from '{}' : {}", source, e.getMessage());
        }
        try {
            zdt.setMaxDepth(Double.parseDouble(tokens[3]));
        } catch (NumberFormatException e) {
            LOGGER.warn("While parsing max depth from '{}' : {}", source, e.getMessage());
        }
        try {
            zdt.setEndDate(LocalDateTime.parse(tokens[4], DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        } catch (Exception e) {
            LOGGER.warn("While parsing end date from '{}' : {}", source, e.getMessage());
        }
        try {
            zdt.setMinTemperature(Double.parseDouble(tokens[5]));
        } catch (NumberFormatException e) {
            LOGGER.warn("While parsing min temperature from '{}' : {}", source, e.getMessage());
        }
        try {
            zdt.setBarsUsed(Integer.parseInt(tokens[6]));
        } catch (NumberFormatException e) {
            LOGGER.warn("While parsing bars used from '{}' : {}", source, e.getMessage());
        }

        return zdt;
    }

    public Zdp parseZdp(String source) {
        ValidationTools.validateNotBlank(source);

        String[] tokens = source.split("\n");

        if(! tokens[0].startsWith(Zdp.TOKEN_HEADER)) {
            illegal("header is invalid: "+tokens[0]);
        }

        if(! tokens[tokens.length-1].startsWith(Zdp.TOKEN_HEADER)) {
            illegal("tail is invalid: "+tokens[tokens.length-1]);
        }

        Zdp zdp = new Zdp();

        for (String token : tokens) {
            if (ZDP_START.equals(token) || ZDP_END.equals(token)) {
                // skip
            } else {
                String[] fields = token.split(Pattern.quote(TOKEN_SEPARATOR));
                ZdpRecord zdpr = new ZdpRecord();
                int index = 1; // skip first, empty token}
                if(index < fields.length) {
                    zdpr.setMinutes(Double.parseDouble(fields[index++]));
                }
                if (zdpr.getMinutes() > 0) {
                    if(index < fields.length) {
                        try {
                            zdpr.setMeters(Double.parseDouble(fields[index++]));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing meters from '{}' : {}", source, e.getMessage());
                        }
                    }
                    if(index < fields.length) {
                        zdpr.setUnknown1(fields[index++]);
                    }
                    if(index < fields.length) {
                        zdpr.setUnknown2(fields[index++]);
                    }
                    if(index < fields.length) {
                        zdpr.setUnknown3(fields[index++]);
                    }
                    if(index < fields.length) {
                        zdpr.setUnknown4(fields[index++]);
                    }
                    if(index < fields.length) {
                        zdpr.setUnknown5(fields[index++]);
                    }
                    if(index < fields.length) {
                        try {
                            zdpr.setDegrees(Double.parseDouble(fields[index++]));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing degrees from '{}' : {}", source, e.getMessage());
                        }
                    }
                    if(index < fields.length) {
                        zdpr.setUnknown6(fields[index++]);
                    }
                    if(index < fields.length) {
                        zdpr.setUnknown7(fields[index++]);
                    }
                    zdp.getSamples().add(zdpr);
                }
            }
        }
        return zdp;
    }

    public Zdh parseZdh(String source) {
        ValidationTools.validateNotBlank(source);
        String[] tokens = source.split(Pattern.quote(TOKEN_SEPARATOR));
        ValidationTools.validateHeader(tokens[0], Zdh.TOKEN_HEADER);

        Zdh zdh = new Zdh();
        try {
            zdh.setUnknown1(Integer.parseInt(tokens[1]));
        } catch (NumberFormatException e) {
            zdh.setUnknown1(null);
            LOGGER.warn("While parsing unknown1 from '"+tokens[1]+"'", source, e.getMessage());
        }
        try {
            zdh.setUnknown2(Integer.parseInt(tokens[2]));
        } catch (NumberFormatException e) {
            zdh.setUnknown2(null);
            LOGGER.warn("While parsing unknown2 from '"+tokens[2]+"'", source, e.getMessage());
        }
        zdh.setUnknown3(tokens[3]);
        zdh.setSampleFrequency(tokens[4]);
        try {
            zdh.setDiveDate(LocalDateTime.parse(tokens[5], DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        } catch (Exception e) {
            LOGGER.warn("While parsing dive date from '"+tokens[5]+"'", source, e.getMessage());
        }
        try {
            zdh.setUnknown4(Double.parseDouble(tokens[6]));
        } catch (NumberFormatException e) {
            zdh.setUnknown4(null);
            LOGGER.warn("While parsing unknown4 from '"+tokens[6]+"'", source, e.getMessage());
        }
        try {
            zdh.setUnknown5(Double.parseDouble(tokens[7]));
        } catch (NumberFormatException e) {
            zdh.setUnknown5(null);
            LOGGER.warn("While parsing unknown5 from '"+tokens[7]+"'", source, e.getMessage());
        }
        zdh.setUnknown6(tokens[8]);
        return zdh;
    }

    public Zrh parseZrh(String source) {
        ValidationTools.validateNotBlank(source);

        String[] tokens = source.split(Pattern.quote(TOKEN_SEPARATOR));

        ValidationTools.validateHeader(tokens[0], Zrh.TOKEN_HEADER);

        return new Zrh()
            .setUnknown1(tokens[1])
            .setUnknown2(tokens[2])
            .setDeviceSerial(tokens[3])
            .setUnknown3(tokens[4])
            .setDistanceUnit(tokens[5])
            .setTemperatureUnit(tokens[6])
            .setPressureUnit(tokens[7])
            .setVolumeUnit(tokens[8]);
    }

    public Fsh parseFsh(String source) {
        ValidationTools.validateNotBlank(source);
        String[] tokens = source.split(Pattern.quote(TOKEN_SEPARATOR));
        ValidationTools.validateHeader(tokens[0], Fsh.TOKEN_HEADER);

        Fsh fsh = new Fsh();
        fsh.setUnknown1(tokens[1]);
        fsh.setUnknown2(tokens[2]);
        fsh.setUnknown3(tokens[3]);
        try {
            fsh.setUploadDate(LocalDateTime.parse(tokens[4], DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        } catch (Exception e) {
            LOGGER.warn("While parsing upload date from '"+tokens[4]+"'", source, e.getMessage());
        }

        return fsh;
    }

    public Zar parseZar(String source) throws IOException {
        ValidationTools.validateNotBlank(source);

        if(!source.startsWith(ZAR_START) && !source.endsWith(ZAR_END)) {
            illegal("source is invalid: " + source);
        }

        source = source.substring(4);
        source = source.substring(0, source.length()-1);
        ZarDto dto = new XmlMapper().readValue(source, ZarDto.class);

        Zar zar = new Zar();
        zar.setApp(dto.getApp());
        zar.setDuid(dto.getDuid());
        zar.setTitle(dto.getTitle());
        try {
            zar.setDiveDate(LocalDateTime.parse(dto.getDiveDate(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        } catch (Exception e) {
            LOGGER.warn("While parsing dive date from '"+dto.getDiveDate()+"'", source, e.getMessage());
        }
        try {
            zar.setFileDate(LocalDateTime.parse(dto.getFileDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        } catch (Exception e) {
            LOGGER.warn("While parsing file date from '"+dto.getFileDate()+"'", source, e.getMessage());
        }
        zar.setDiveMode(dto.getDiveMode());
        zar.setPdcModel(dto.getPdcModel());
        zar.setPdcSerial(dto.getPdcSerial());
        zar.setManufacturer(dto.getManufacturer());
        zar.setPdcFirmware(dto.getPdcFirmware());
        zar.setDiverName(dto.getDiverName());
        zar.setLocation(parseLocation(dto.getLocation()));
        zar.setGear(parseGear(dto.getGear()));
        zar.setRating(dto.getRating());
        zar.setDiveStats(parseDiveStats(dto.getDiveStats()));
        zar.setTank(parseTank(dto.getTank()));
        zar.setDiveMemo(dto.getDiveMemo());
        zar.setAscentRateBarGraph(parseIntArray(dto.getArBg()));
        zar.setTissueLoadingBarGraph(parseIntArray(dto.getTlBg()));
        zar.setO2loadingBarGraph(parseIntArray(dto.getO2Bg()));
        zar.setAirTimeRemaining(parseIntArray(dto.getAtr()));
        zar.setDiveTimeRemaining(parseIntArray(dto.getDtr()));
        zar.setDecoTime(parseIntArray(dto.getDecoTime()));
        zar.setLinked(parseIntArray(dto.getLinked()));
        zar.setVStatus(parseIntArray(dto.getVstatus()));
        zar.setVariFlash(parseIntArray(dto.getVariFlash()));
        zar.setSetpoints(parseSetPoints(dto.getSetPoints()));
        zar.setImageNames(parseImages(dto.getImages()));
        zar.setSignatureImageName(dto.getSignatureFile());
        return zar;
    }

    private List<String> parseImages(String images) throws IOException {
        List<String> imgs = new ArrayList<>();
        String target = null;
        if(StringUtils.isNotBlank(images)) {
            String[] tokens = images.split("=");
            boolean done = false;
            for(int i = 0; i < tokens.length && !done; i++) {
                if("DATA".equals(tokens[i])) {
                    target = tokens[i+1];
                    done = true;
                }
            }

            if(StringUtils.isNotBlank(target)) {
                target = target.replaceAll(Pattern.quote("["), "").replaceAll(Pattern.quote("]"), "");
                imgs.addAll(Arrays.asList(target.split(",")));
            }
        }
        return imgs;
    }

    private String clean(String s) {
        return s == null ? s : s.replaceAll(Pattern.quote("["), "")
                .replaceAll(Pattern.quote("]"), "");
    }

    private String[] splitValueToken(String valueToken) {
        String[] toks = valueToken.split(",");
        List<String> acc = new ArrayList<>(toks.length);

        for (Integer i = 0; i < toks.length; i++) {
            String curr = toks[i];
            if(!curr.contains("=")) { // split token (used to contain a ',')
                if(i == 0) {
                    runtime("Split token at index 0");
                } else {
                    String prev = acc.remove(i-1);
                    acc.add(prev+","+curr);
                }
            } else {
                acc.add(curr);
            }
        }

        return acc.toArray(new String[]{});
    }

    private Integer[] parseIntArray(String arBg) {

        if(StringUtils.isNotBlank(arBg)) {
            String[] tokens = arBg.split(",");

            List<Integer> vals = Arrays.asList(tokens).stream()
                    .map(t -> Integer.parseInt(t.trim()))
                    .collect(Collectors.toList());
            return vals.toArray(new Integer[]{});
        }
        return new Integer[]{};
    }

    private Setpoints parseSetPoints(String setPointsString) {
        if(StringUtils.isNotBlank(setPointsString)) {
            String[] tokens = splitValueToken(setPointsString);

            Setpoints setpoints = new Setpoints();

            for (String token : tokens) {
                String[] field = token.split("=");
                String name = field[0];
                String source = clean(field[1]);

                try {
                    switch (SetpointsFields.valueOf(name)) {
                        case HOUR:
                            try {
                                setpoints.setHour(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing hour from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case DATE:
                            setpoints.setDateFormat(source);
                            break;
                        case AUDIBLE:
                            setpoints.setAudible(source);
                            break;
                        case EDT:
                            try {
                                setpoints.setEdt(Double.parseDouble(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing edt from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case DTRON:
                            setpoints.setDtron(source);
                            break;
                        case DTR:
                            try {
                                setpoints.setDtr(Double.parseDouble(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing dtr from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case MAXTLBGON:
                            setpoints.setMaxTlbgon(source);
                            break;
                        case MAXTLBG:
                            break;
                        case FREEDEPTH1ON:
                            setpoints.setFreeDepth1on(source);
                            break;
                        case FREEDEPTH1:
                            break;
                        case FREEDEPTH2ON:
                            setpoints.setFreeDepth2on(source);
                            break;
                        case FREEDEPTH2:
                            break;
                        case FREEDEPTH3ON:
                            setpoints.setFreeDepth3on(source);
                            break;
                        case FREEDEPTH3:
                            break;
                        case FREEEDTON:
                            setpoints.setFreeEdton(source);
                            break;
                        case FREEEDT:
                            break;
                        case UNITS:
                            try {
                                setpoints.setUnits(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing units from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case WET:
                            setpoints.setWet(source);
                            break;
                        case FO2DEFAULT:
                            setpoints.setFo2default(source);
                            break;
                        case DEEPSTOP:
                            setpoints.setDeepStop(source);
                            break;
                        case CONSERVE:
                            setpoints.setConserve(source);
                            break;
                        case ALGORITHM:
                            try {
                                setpoints.setAlgorithm(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing algorithm from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case SSTOP:
                            try {
                                setpoints.setSsTop(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing SStop from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case SSTIME:
                            try {
                                setpoints.setSsTime(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing SsTime from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case SSDEPTH:
                            try {
                                setpoints.setSsDepth(Double.parseDouble(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing SsDepth from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case BLDURATION:
                            try {
                                setpoints.setBlDuration(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing BlDuration from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case SAMPLE:
                            try {
                                setpoints.setSample(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing sample from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        default:
                            runtime("Invalid setPoints Field: " + name + " in " + setPointsString);
                    }
                } catch (IllegalArgumentException e1) {
                    runtime("Invalid setPoints Field: " + name + " in " + setPointsString);
                }
            }

            return setpoints;
        }
        return null;
    }

    private Tank parseTank(String tankString) {
        if(StringUtils.isNotBlank(tankString)) {
            String[] tokens = splitValueToken(tankString);

            Tank tank = new Tank();

            for (String token : tokens) {
                String[] field = token.split("=");
                String name = field[0];
                String source = clean(field[1]);
                try {
                    switch (TankFields.valueOf(name)) {
                        case NUMBER:
                            try {
                                tank.setNumber(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing tank number from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case TID:
                            try {
                                tank.setTid(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing tid from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case ON:
                            tank.setOn(source);
                            break;
                        case CYLNAME:
                            tank.setCylName(source);
                            break;
                        case CYLSIZE:
                            tank.setCylSize(source);
                            break;
                        case WORKINGPRESSURE:
                            tank.setWorkingPressure(source);
                            break;
                        case STARTPRESSURE:
                            try {
                                tank.setStartPressure(Double.parseDouble(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing start pressure from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case ENDPRESSURE:
                            try {
                                tank.setEndPressure(Double.parseDouble(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing end pressure from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case FO2:
                            try {
                                tank.setfO2(Integer.parseInt(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing FO2 from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case AVGDEPTH:
                            try {
                                tank.setAvgDepth(Double.parseDouble(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing avg depth from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case DIVETIME:
                            try {
                                tank.setDiveTime(Double.parseDouble(source));
                            } catch (NumberFormatException e) {
                                LOGGER.warn("While parsing dive time from '{}' : {}", source, e.getMessage());
                            }
                            break;
                        case SAC:
                            tank.setSac(source);
                            break;
                        default:
                            throw new RuntimeException("Invalid Tank Field: " + name + " in " + tankString);
                    }
                } catch (IllegalArgumentException e1) {
                    runtime("Invalid tank Field: " + name + " in " + tankString);
                }
            }

            return tank;
        }
        return null;
    }

    private DiveStats parseDiveStats(String diveStatsString) {
        ValidationTools.validateNotBlank(diveStatsString);

        String[] tokens = splitValueToken(diveStatsString);

        DiveStats stats = new DiveStats();

        for (String token : tokens) {
            String[] field = token.split("=");
            String name = field[0];
            String source = clean(field[1]);

            try {
                switch (DiveStatsFields.valueOf(name.replaceAll("/", "_"))) {
                    case DIVENO:
                        try {
                            stats.setDiveNo(Integer.parseInt(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing dive no from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case DATATYPE:
                        try {
                            stats.setDataType(Integer.parseInt(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing data type from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case DECO:
                        stats.setDeco(source);
                        break;
                    case VIOL:
                        stats.setViol(source);
                        break;
                    case MODE:
                        try {
                            stats.setMode(Integer.parseInt(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing mode from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case MANUALDIVE:
                        try {
                            stats.setManualDive(Integer.parseInt(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing manual dive from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case EDT:
                        stats.setEdt(source);
                        break;
                    case SI:
                        stats.setSi(source);
                        break;
                    case MAXDEPTH:
                        try {
                            stats.setMaxDepth(Double.parseDouble(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing max depth from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case MAXO2:
                        try {
                            stats.setMaxO2(Double.parseDouble(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing max O2 from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case PO2:
                        try {
                            stats.setP02(Double.parseDouble(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing PO2 from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case MINTEMP:
                        try {
                            stats.setMinTemp(Double.parseDouble(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing min temp from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    default:
                        runtime("Invalid Dive Stat Field: " + name + " in " + diveStatsString);
                }
            } catch (IllegalArgumentException e1) {
                runtime("Invalid dive stat Field: " + name + " in " + diveStatsString);
            }
        }

        return stats;
    }

    private Gear parseGear(String gearString) {
        ValidationTools.validateNotBlank(gearString);

        String[] tokens = splitValueToken(gearString);

        Gear gear = new Gear();

        for (String token : tokens) {
            String[] field = token.split("=");
            String name = field[0];
            String source = clean(field[1]);

            try {
                switch (GearFields.valueOf(name.replaceAll("/", "_"))) {
                    case GEAR_UNITS:
                        try {
                            gear.setGearUnits(Integer.parseInt(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing gear units from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case NAME:
                        gear.setName(source);
                        break;
                    case WEIGHTBELT:
                        try {
                            gear.setWeightBelt(Double.parseDouble(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing weight belt from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case SUIT:
                        gear.setSuit(source);
                        break;
                    default:
                        runtime("Invalid Gear Unit: " + name + " in " + gearString);
                }
            } catch (IllegalArgumentException e1) {
                runtime("Invalid dive gear Field: " + name + " in " + gearString);
            }
        }

        return gear;
    }

    private Location parseLocation(String locationString) {
        ValidationTools.validateNotBlank(locationString);

        String[] tokens = splitValueToken(locationString);

        Location loc = new Location();

        for (String token : tokens) {
            String[] field = token.split("=");
            String name = field[0];
            String source = clean(field[1]);
            try {
                switch (LocationFields.valueOf(name.replaceAll("/", "_"))) {
                    case GPS:
                        String[] coords = source.split(",");
                        try {
                            loc.setLatitude(Double.parseDouble(coords[0]));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing latitude from '"+coords[0]+"'", source, e.getMessage());
                        }
                        try {
                            loc.setLongitude(Double.parseDouble(coords[1]));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing longitude from '"+coords[1]+"'", source, e.getMessage());
                        }
                        break;
                    case LOCNAME:
                        loc.setLocName(source);
                        break;
                    case CITY:
                        loc.setCity(source);
                        break;
                    case STATE_PROVINCE:
                        loc.setState(source);
                        break;
                    case COUNTRY:
                        loc.setCountry(source);
                        break;
                    case AIRTEMP:
                        try {
                            loc.setAirTemp(Double.parseDouble(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While parsing air temp from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case SURFACETEMP:
                        try {
                            loc.setSurfaceTemp(Double.parseDouble(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While surface temp from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case MINTEMP:
                        try {
                            loc.setMinTemp(Double.parseDouble(source));
                        } catch (NumberFormatException e) {
                            LOGGER.warn("While min temp from '{}' : {}", source, e.getMessage());
                        }
                        break;
                    case VISIBILITY:
                        loc.setVisibility(source);
                        break;
                    default:
                        throw new RuntimeException("Unknown Location Field: " + name + " in " + locationString);
                }
            } catch (IllegalArgumentException e1) {
                throw new RuntimeException("Invalid dive location Field: " + name + " in " + locationString);
            }
        }

        return loc;
    }
}
