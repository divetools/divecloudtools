package com.gratteburnes.divetools.divecloud.parse.model.zdt;

import java.time.LocalDateTime;

public class Zdt {
    public static final String TOKEN_HEADER = "ZDT";

    private Integer unknown1;
    private Integer unknown2;
    private Double maxDepth;
    private LocalDateTime endDate;
    private Double minTemperature;
    private Integer barsUsed;

    public Zdt(){

    }

    public Integer getUnknown1() {
        return unknown1;
    }

    public Zdt setUnknown1(Integer unknown1) {
        this.unknown1 = unknown1;
        return this;
    }

    public Integer getUnknown2() {
        return unknown2;
    }

    public Zdt setUnknown2(Integer unknown2) {
        this.unknown2 = unknown2;
        return this;
    }

    public Double getMaxDepth() {
        return maxDepth;
    }

    public Zdt setMaxDepth(Double maxDepth) {
        this.maxDepth = maxDepth;
        return this;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public Zdt setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public Double getMinTemperature() {
        return minTemperature;
    }

    public Zdt setMinTemperature(Double minTemperature) {
        this.minTemperature = minTemperature;
        return this;
    }

    public Integer getBarsUsed() {
        return barsUsed;
    }

    public Zdt setBarsUsed(Integer barsUsed) {
        this.barsUsed = barsUsed;
        return this;
    }
}
