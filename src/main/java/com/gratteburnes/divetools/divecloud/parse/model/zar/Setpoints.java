package com.gratteburnes.divetools.divecloud.parse.model.zar;

public class Setpoints {
    private Integer hour;
    private String dateFormat;
    private String audible;
    private Double edt;
    private String dtron;

    public Double getDtr() {
        return dtr;
    }

    public Setpoints setDtr(Double dtr) {
        this.dtr = dtr;
        return this;
    }

    private Double dtr;
    private String maxTlbgon;
    private Object maxTlbg;
    private String freeDepth1on;
    private Object freeDepth1;
    private String freeDepth2on;
    private Object freeDepth2;
    private String freeDepth3on;
    private Object freeDepth3;
    private String freeEdton;
    private Object freeDt;
    private Integer units;
    private String wet;
    private String fo2default;
    private String deepStop;
    private String conserve;
    private Integer algorithm;
    private Integer ssTop;
    private Integer ssTime;
    private Double ssDepth;
    private Integer blDuration;
    private Integer sample;

    public Setpoints() {

    }

    public Object getMaxTlbg() {
        return maxTlbg;
    }

    public Setpoints setMaxTlbg(Object maxTlbg) {
        this.maxTlbg = maxTlbg;
        return this;
    }

    public Object getFreeDepth1() {
        return freeDepth1;
    }

    public Setpoints setFreeDepth1(Object freeDepth1) {
        this.freeDepth1 = freeDepth1;
        return this;
    }

    public Object getFreeDepth2() {
        return freeDepth2;
    }

    public Setpoints setFreeDepth2(Object freeDepth2) {
        this.freeDepth2 = freeDepth2;
        return this;
    }

    public Object getFreeDepth3() {
        return freeDepth3;
    }

    public Setpoints setFreeDepth3(Object freeDepth3) {
        this.freeDepth3 = freeDepth3;
        return this;
    }

    public Object getFreeDt() {
        return freeDt;
    }

    public Setpoints setFreeDt(Object freeDt) {
        this.freeDt = freeDt;
        return this;
    }

    public Integer getHour() {
        return hour;
    }

    public Setpoints setHour(Integer hour) {
        this.hour = hour;
        return this;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public Setpoints setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
        return this;
    }

    public String getAudible() {
        return audible;
    }

    public Setpoints setAudible(String audible) {
        this.audible = audible;
        return this;
    }

    public Double getEdt() {
        return edt;
    }

    public Setpoints setEdt(Double edt) {
        this.edt = edt;
        return this;
    }

    public String getDtron() {
        return dtron;
    }

    public Setpoints setDtron(String dtron) {
        this.dtron = dtron;
        return this;
    }

    public String getMaxTlbgon() {
        return maxTlbgon;
    }

    public Setpoints setMaxTlbgon(String maxTlbgon) {
        this.maxTlbgon = maxTlbgon;
        return this;
    }

    public String getFreeDepth1on() {
        return freeDepth1on;
    }

    public Setpoints setFreeDepth1on(String freeDepth1on) {
        this.freeDepth1on = freeDepth1on;
        return this;
    }

    public String getFreeDepth2on() {
        return freeDepth2on;
    }

    public Setpoints setFreeDepth2on(String freeDepth2on) {
        this.freeDepth2on = freeDepth2on;
        return this;
    }

    public String getFreeDepth3on() {
        return freeDepth3on;
    }

    public Setpoints setFreeDepth3on(String freeDepth3on) {
        this.freeDepth3on = freeDepth3on;
        return this;
    }

    public String getFreeEdton() {
        return freeEdton;
    }

    public Setpoints setFreeEdton(String freeEdton) {
        this.freeEdton = freeEdton;
        return this;
    }

    public Integer getUnits() {
        return units;
    }

    public Setpoints setUnits(Integer units) {
        this.units = units;
        return this;
    }

    public String getWet() {
        return wet;
    }

    public Setpoints setWet(String wet) {
        this.wet = wet;
        return this;
    }

    public String getFo2default() {
        return fo2default;
    }

    public Setpoints setFo2default(String fo2default) {
        this.fo2default = fo2default;
        return this;
    }

    public String getDeepStop() {
        return deepStop;
    }

    public Setpoints setDeepStop(String deepStop) {
        this.deepStop = deepStop;
        return this;
    }

    public String getConserve() {
        return conserve;
    }

    public Setpoints setConserve(String conserve) {
        this.conserve = conserve;
        return this;
    }

    public Integer getAlgorithm() {
        return algorithm;
    }

    public Setpoints setAlgorithm(Integer algorithm) {
        this.algorithm = algorithm;
        return this;
    }

    public Integer getSsTop() {
        return ssTop;
    }

    public Setpoints setSsTop(Integer ssTop) {
        this.ssTop = ssTop;
        return this;
    }

    public Integer getSsTime() {
        return ssTime;
    }

    public Setpoints setSsTime(Integer ssTime) {
        this.ssTime = ssTime;
        return this;
    }

    public Double getSsDepth() {
        return ssDepth;
    }

    public Setpoints setSsDepth(Double ssDepth) {
        this.ssDepth = ssDepth;
        return this;
    }

    public Integer getBlDuration() {
        return blDuration;
    }

    public Setpoints setBlDuration(Integer blDuration) {
        this.blDuration = blDuration;
        return this;
    }

    public Integer getSample() {
        return sample;
    }

    public Setpoints setSample(Integer sample) {
        this.sample = sample;
        return this;
    }
}
