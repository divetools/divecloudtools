package com.gratteburnes.divetools.divecloud.parse.model.zar;

import java.time.LocalDateTime;
import java.util.List;

public class Zar {
    private String app;
    private String duid;
    private String title;
    private LocalDateTime diveDate;
    private LocalDateTime fileDate;
    private Integer diveMode;
    private String pdcModel;
    private String pdcSerial;
    private String manufacturer;
    private String pdcFirmware;
    private String diverName;
    private Location location;
    private Gear gear;
    private Integer rating;
    private DiveStats diveStats;
    private Tank tank;
    private String diveMemo;
    private Integer[] ascentRateBarGraph;
    private Integer[] tissueLoadingBarGraph;
    private Integer[] o2loadingBarGraph;
    private Integer[] airTimeRemaining;
    private Integer[] diveTimeRemaining;
    private Integer[] decoTime;
    private Integer[] linked;
    private Integer[] vStatus;
    private Integer[] variFlash;
    private Setpoints setpoints;
    private List<String> imageNames;
    private String signatureImageName;

    public Zar() {

    }

    public String getApp() {
        return app;
    }

    public Zar setApp(String app) {
        this.app = app;
        return this;
    }

    public String getDuid() {
        return duid;
    }

    public Zar setDuid(String duid) {
        this.duid = duid;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Zar setTitle(String title) {
        this.title = title;
        return this;
    }

    public LocalDateTime getDiveDate() {
        return diveDate;
    }

    public Zar setDiveDate(LocalDateTime diveDate) {
        this.diveDate = diveDate;
        return this;
    }

    public LocalDateTime getFileDate() {
        return fileDate;
    }

    public Zar setFileDate(LocalDateTime fileDate) {
        this.fileDate = fileDate;
        return this;
    }

    public Integer getDiveMode() {
        return diveMode;
    }

    public Zar setDiveMode(Integer diveMode) {
        this.diveMode = diveMode;
        return this;
    }

    public String getPdcModel() {
        return pdcModel;
    }

    public Zar setPdcModel(String pdcModel) {
        this.pdcModel = pdcModel;
        return this;
    }

    public String getPdcSerial() {
        return pdcSerial;
    }

    public Zar setPdcSerial(String pdcSerial) {
        this.pdcSerial = pdcSerial;
        return this;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public Zar setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
        return this;
    }

    public String getPdcFirmware() {
        return pdcFirmware;
    }

    public Zar setPdcFirmware(String pdcFirmware) {
        this.pdcFirmware = pdcFirmware;
        return this;
    }

    public String getDiverName() {
        return diverName;
    }

    public Zar setDiverName(String diverName) {
        this.diverName = diverName;
        return this;
    }

    public Location getLocation() {
        return location;
    }

    public Zar setLocation(Location location) {
        this.location = location;
        return this;
    }

    public Gear getGear() {
        return gear;
    }

    public Zar setGear(Gear gear) {
        this.gear = gear;
        return this;
    }

    public Integer getRating() {
        return rating;
    }

    public Zar setRating(Integer rating) {
        this.rating = rating;
        return this;
    }

    public DiveStats getDiveStats() {
        return diveStats;
    }

    public Zar setDiveStats(DiveStats diveStats) {
        this.diveStats = diveStats;
        return this;
    }

    public Tank getTank() {
        return tank;
    }

    public Zar setTank(Tank tank) {
        this.tank = tank;
        return this;
    }

    public String getDiveMemo() {
        return diveMemo;
    }

    public Zar setDiveMemo(String diveMemo) {
        this.diveMemo = diveMemo;
        return this;
    }

    public Integer[] getAscentRateBarGraph() {
        return ascentRateBarGraph;
    }

    public Zar setAscentRateBarGraph(Integer[] ascentRateBarGraph) {
        this.ascentRateBarGraph = ascentRateBarGraph;
        return this;
    }

    public Integer[] getTissueLoadingBarGraph() {
        return tissueLoadingBarGraph;
    }

    public Zar setTissueLoadingBarGraph(Integer[] tissueLoadingBarGraph) {
        this.tissueLoadingBarGraph = tissueLoadingBarGraph;
        return this;
    }

    public Integer[] getO2loadingBarGraph() {
        return o2loadingBarGraph;
    }

    public Zar setO2loadingBarGraph(Integer[] o2loadingBarGraph) {
        this.o2loadingBarGraph = o2loadingBarGraph;
        return this;
    }

    public Integer[] getAirTimeRemaining() {
        return airTimeRemaining;
    }

    public Zar setAirTimeRemaining(Integer[] airTimeRemaining) {
        this.airTimeRemaining = airTimeRemaining;
        return this;
    }

    public Integer[] getDiveTimeRemaining() {
        return diveTimeRemaining;
    }

    public Zar setDiveTimeRemaining(Integer[] diveTimeRemaining) {
        this.diveTimeRemaining = diveTimeRemaining;
        return this;
    }

    public Integer[] getDecoTime() {
        return decoTime;
    }

    public Zar setDecoTime(Integer[] decoTime) {
        this.decoTime = decoTime;
        return this;
    }

    public Integer[] getLinked() {
        return linked;
    }

    public Zar setLinked(Integer[] linked) {
        this.linked = linked;
        return this;
    }

    public Integer[] getVStatus() {
        return vStatus;
    }

    public Zar setVStatus(Integer[] vStatus) {
        this.vStatus = vStatus;
        return this;
    }

    public Integer[] getVariFlash() {
        return variFlash;
    }

    public Zar setVariFlash(Integer[] variFlash) {
        this.variFlash = variFlash;
        return this;
    }

    public Setpoints getSetpoints() {
        return setpoints;
    }

    public Zar setSetpoints(Setpoints setpoints) {
        this.setpoints = setpoints;
        return this;
    }


    public Zar setImageNames(List<String> imageNames) {
        this.imageNames = imageNames;
        return this;
    }

    public List<String> getImageNames() {
        return imageNames;
    }

    public String getSignatureImageName() {
        return signatureImageName;
    }

    public Zar setSignatureImageName(String signatureImageName) {
        this.signatureImageName = signatureImageName;
        return this;
    }
}
