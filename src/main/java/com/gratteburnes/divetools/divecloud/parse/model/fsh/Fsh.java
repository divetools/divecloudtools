package com.gratteburnes.divetools.divecloud.parse.model.fsh;

import java.time.LocalDateTime;

public class Fsh {
    public static final String TOKEN_HEADER = "FSH";

    private String unknown1;
    private String unknown2;
    private String unknown3;
    private LocalDateTime uploadDate;

    public Fsh() {

    }

    public String getUnknown1() {
        return unknown1;
    }

    public Fsh setUnknown1(String unknown1) {
        this.unknown1 = unknown1;
        return this;
    }

    public String getUnknown2() {
        return unknown2;
    }

    public Fsh setUnknown2(String unknown2) {
        this.unknown2 = unknown2;
        return this;
    }

    public String getUnknown3() {
        return unknown3;
    }

    public Fsh setUnknown3(String unknown3) {
        this.unknown3 = unknown3;
        return this;
    }

    public LocalDateTime getUploadDate() {
        return uploadDate;
    }

    public Fsh setUploadDate(LocalDateTime uploadDate) {
        this.uploadDate = uploadDate;
        return this;
    }
}
