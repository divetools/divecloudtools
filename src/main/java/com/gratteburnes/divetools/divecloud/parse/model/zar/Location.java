package com.gratteburnes.divetools.divecloud.parse.model.zar;

public class Location {
    private Double latitude;
    private Double longitude;
    private String locName;
    private String city;
    private String state;
    private String country;
    private Double airTemp;
    private Double surfaceTemp;
    private Double minTemp;
    private String visibility;

    public String getVisibility() {
        return visibility;
    }

    public Location setVisibility(String visibility) {
        this.visibility = visibility;
        return this;
    }

    public Location() {

    }

    public Double getLatitude() {
        return latitude;
    }

    public Location setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Location setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getLocName() {
        return locName;
    }

    public Location setLocName(String locName) {
        this.locName = locName;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Location setCity(String city) {
        this.city = city;
        return this;
    }

    public String getState() {
        return state;
    }

    public Location setState(String state) {
        this.state = state;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Location setCountry(String country) {
        this.country = country;
        return this;
    }

    public Double getAirTemp() {
        return airTemp;
    }

    public Location setAirTemp(Double airTemp) {
        this.airTemp = airTemp;
        return this;
    }

    public Double getSurfaceTemp() {
        return surfaceTemp;
    }

    public Location setSurfaceTemp(Double surfaceTemp) {
        this.surfaceTemp = surfaceTemp;
        return this;
    }

    public Double getMinTemp() {
        return minTemp;
    }

    public Location setMinTemp(Double minTemp) {
        this.minTemp = minTemp;
        return this;
    }
}
