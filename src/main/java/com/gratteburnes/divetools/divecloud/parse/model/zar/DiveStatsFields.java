package com.gratteburnes.divetools.divecloud.parse.model.zar;

public enum DiveStatsFields {
    DIVENO,
    DATATYPE,
    DECO,
    VIOL,
    MODE,
    MANUALDIVE,
    EDT,
    SI,
    MAXDEPTH,
    MAXO2,
    PO2,
    MINTEMP
}
