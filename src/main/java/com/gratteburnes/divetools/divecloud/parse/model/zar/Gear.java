package com.gratteburnes.divetools.divecloud.parse.model.zar;

public class Gear {
    private Integer gearUnits;
    private Double weightBelt;
    private String name;
    private String suit;

    public Gear() {

    }

    public Integer getGearUnits() {
        return gearUnits;
    }

    public Gear setGearUnits(Integer gearUnits) {
        this.gearUnits = gearUnits;
        return this;
    }

    public Double getWeightBelt() {
        return weightBelt;
    }

    public Gear setWeightBelt(Double weightBelt) {
        this.weightBelt = weightBelt;
        return this;
    }

    public String getName() {
        return name;
    }

    public Gear setName(String name) {
        this.name = name;
        return this;
    }

    public String getSuit() {
        return suit;
    }

    public Gear setSuit(String suit) {
        this.suit = suit;
        return this;
    }
}
