package com.gratteburnes.divetools.divecloud.parse.model.zdh;

import java.time.LocalDateTime;

public class Zdh {
    public static final String TOKEN_HEADER = "ZDH";

    private Integer unknown1;
    private Integer unknown2;
    private String unknown3;
    private String sampleFrequency;
    private LocalDateTime diveDate;
    private Double unknown4;
    private Double unknown5;
    private String unknown6;

    public Zdh() {

    }

    public Integer getUnknown1() {
        return unknown1;
    }

    public Zdh setUnknown1(Integer unknown1) {
        this.unknown1 = unknown1;
        return this;
    }

    public Integer getUnknown2() {
        return unknown2;
    }

    public Zdh setUnknown2(Integer unknown2) {
        this.unknown2 = unknown2;
        return this;
    }

    public String getUnknown3() {
        return unknown3;
    }

    public Zdh setUnknown3(String unknown3) {
        this.unknown3 = unknown3;
        return this;
    }

    public String getSampleFrequency() {
        return sampleFrequency;
    }

    public Zdh setSampleFrequency(String sampleFrequency) {
        this.sampleFrequency = sampleFrequency;
        return this;
    }

    public LocalDateTime getDiveDate() {
        return diveDate;
    }

    public Zdh setDiveDate(LocalDateTime diveDate) {
        this.diveDate = diveDate;
        return this;
    }

    public double getUnknown4() {
        return unknown4;
    }

    public Zdh setUnknown4(Double unknown4) {
        this.unknown4 = unknown4;
        return this;
    }

    public Double getUnknown5() {
        return unknown5;
    }

    public Zdh setUnknown5(Double unknown5) {
        this.unknown5 = unknown5;
        return this;
    }

    public String getUnknown6() {
        return unknown6;
    }

    public Zdh setUnknown6(String unknown6) {
        this.unknown6 = unknown6;
        return this;
    }
}
