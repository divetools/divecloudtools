package com.gratteburnes.divetools.divecloud.parse.model.zar;

public enum TankFields {
    NUMBER,
    TID,
    ON,
    CYLNAME,
    CYLSIZE,
    WORKINGPRESSURE,
    STARTPRESSURE,
    ENDPRESSURE,
    FO2,
    AVGDEPTH,
    DIVETIME,
    SAC
}
