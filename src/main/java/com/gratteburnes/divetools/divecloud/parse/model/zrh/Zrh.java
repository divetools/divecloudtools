package com.gratteburnes.divetools.divecloud.parse.model.zrh;

public class Zrh {
    public static final String TOKEN_HEADER = "ZRH";

    private String unknown1;
    private String deviceSerial;
    private String unknown2;
    private String unknown3;
    private String distanceUnit;
    private String temperatureUnit;
    private String pressureUnit;
    private String volumeUnit;

    public Zrh() {

    }

    public String getUnknown1() {
        return unknown1;
    }

    public Zrh setUnknown1(String unknown1) {
        this.unknown1 = unknown1;
        return this;
    }

    public String getDeviceSerial() {
        return deviceSerial;
    }

    public Zrh setDeviceSerial(String deviceSerial) {
        this.deviceSerial = deviceSerial;
        return this;
    }

    public String getUnknown2() {
        return unknown2;
    }

    public Zrh setUnknown2(String unknown2) {
        this.unknown2 = unknown2;
        return this;
    }

    public String getUnknown3() {
        return unknown3;
    }

    public Zrh setUnknown3(String unknown3) {
        this.unknown3 = unknown3;
        return this;
    }

    public String getTemperatureUnit() {
        return temperatureUnit;
    }

    public Zrh setTemperatureUnit(String temperatureUnit) {
        this.temperatureUnit = temperatureUnit;
        return this;
    }

    public String getPressureUnit() {
        return pressureUnit;
    }

    public Zrh setPressureUnit(String pressureUnit) {
        this.pressureUnit = pressureUnit;
        return this;
    }

    public String getVolumeUnit() {
        return volumeUnit;
    }

    public Zrh setVolumeUnit(String volumeUnit) {
        this.volumeUnit = volumeUnit;
        return this;
    }

    public String getDistanceUnit() {
        return distanceUnit;
    }

    public Zrh setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
        return this;
    }
}
