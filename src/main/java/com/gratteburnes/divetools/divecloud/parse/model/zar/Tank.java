package com.gratteburnes.divetools.divecloud.parse.model.zar;

public class Tank {
    private Integer number;
    private Integer tid;
    private String on;
    private String cylName;
    private String cylSize;
    private String workingPressure;
    private Double startPressure;
    private Double endPressure;
    private Integer fO2;
    private Double avgDepth;
    private Double diveTime;
    private String sac;

    public Tank() {

    }

    public Integer getNumber() {
        return number;
    }

    public Tank setNumber(Integer number) {
        this.number = number;
        return this;
    }

    public Integer getTid() {
        return tid;
    }

    public Tank setTid(Integer tid) {
        this.tid = tid;
        return this;
    }

    public String getOn() {
        return on;
    }

    public Tank setOn(String on) {
        this.on = on;
        return this;
    }

    public String getCylName() {
        return cylName;
    }

    public Tank setCylName(String cylName) {
        this.cylName = cylName;
        return this;
    }

    public String getCylSize() {
        return cylSize;
    }

    public Tank setCylSize(String cylSize) {
        this.cylSize = cylSize;
        return this;
    }

    public String getWorkingPressure() {
        return workingPressure;
    }

    public Tank setWorkingPressure(String workingPressure) {
        this.workingPressure = workingPressure;
        return this;
    }

    public Double getStartPressure() {
        return startPressure;
    }

    public Tank setStartPressure(Double startPressure) {
        this.startPressure = startPressure;
        return this;
    }

    public Double getEndPressure() {
        return endPressure;
    }

    public Tank setEndPressure(Double endPressure) {
        this.endPressure = endPressure;
        return this;
    }

    public Integer getfO2() {
        return fO2;
    }

    public Tank setfO2(Integer fO2) {
        this.fO2 = fO2;
        return this;
    }

    public Double getAvgDepth() {
        return avgDepth;
    }

    public Tank setAvgDepth(Double avgDepth) {
        this.avgDepth = avgDepth;
        return this;
    }

    public Double getDiveTime() {
        return diveTime;
    }

    public Tank setDiveTime(Double diveTime) {
        this.diveTime = diveTime;
        return this;
    }

    public String getSac() {
        return sac;
    }

    public Tank setSac(String sac) {
        this.sac = sac;
        return this;
    }
}
