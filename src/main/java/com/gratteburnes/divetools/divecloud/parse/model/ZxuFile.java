package com.gratteburnes.divetools.divecloud.parse.model;

import com.gratteburnes.divetools.divecloud.parse.model.fsh.Fsh;
import com.gratteburnes.divetools.divecloud.parse.model.zar.Zar;
import com.gratteburnes.divetools.divecloud.parse.model.zdh.Zdh;
import com.gratteburnes.divetools.divecloud.parse.model.zdp.Zdp;
import com.gratteburnes.divetools.divecloud.parse.model.zdt.Zdt;
import com.gratteburnes.divetools.divecloud.parse.model.zrh.Zrh;

import java.util.Map;

public class ZxuFile {
    private Fsh fsh;
    private Zrh zrh;
    private Zar zar;
    private Zdh zdh;
    private Zdp zdp;
    private Zdt zdt;

    private Map<String, byte[]> images;
    private byte[] signature;

    public Fsh getFsh() {
        return fsh;
    }

    public Zrh getZrh() {
        return zrh;
    }

    public Zar getZar() {
        return zar;
    }
    public Zdh getZdh() {
        return zdh;
    }

    public Zdp getZdp() {
        return zdp;
    }

    public Zdt getZdt() {
        return zdt;
    }

    public Map<String, byte[]> getImages() {
        return images;
    }

    public ZxuFile setZrh(Zrh zrh) {
        this.zrh = zrh;
        return this;
    }

    public ZxuFile setZar(Zar zar) {
        this.zar = zar;
        return this;
    }

    public ZxuFile setZdh(Zdh zdh) {
        this.zdh = zdh;
        return this;
    }

    public ZxuFile setZdp(Zdp zdp) {
        this.zdp = zdp;
        return this;
    }

    public ZxuFile setZdt(Zdt zdt) {
        this.zdt = zdt;
        return this;
    }

    public ZxuFile setFsh(Fsh fsh) {

        this.fsh = fsh;
        return this;
    }

    public ZxuFile setImages(Map<String, byte[]> images) {
        this.images = images;
        return this;
    }

    public byte[] getSignature() {
        return signature;
    }

    public ZxuFile setSignature(byte[] signature) {
        this.signature = signature;
        return this;
    }
}
