package com.gratteburnes.divetools.divecloud.parse.model.zdp;

public class ZdpRecord {
    private Double minutes;
    private Double meters;
    private String unknown1;
    private String unknown2;
    private String unknown3;
    private String unknown4;
    private String unknown5;
    private Double degrees;
    private String unknown6;
    private String unknown7;

    public Double getMinutes() {
        return minutes;
    }

    public ZdpRecord setMinutes(Double minutes) {
        this.minutes = minutes;
        return this;
    }

    public Double getMeters() {
        return meters;
    }

    public ZdpRecord setMeters(Double meters) {
        this.meters = meters;
        return this;
    }

    public String getUnknown1() {
        return unknown1;
    }

    public ZdpRecord setUnknown1(String unknown1) {
        this.unknown1 = unknown1;
        return this;
    }

    public String getUnknown2() {
        return unknown2;
    }

    public ZdpRecord setUnknown2(String unknown2) {
        this.unknown2 = unknown2;
        return this;
    }

    public String getUnknown3() {
        return unknown3;
    }

    public ZdpRecord setUnknown3(String unknown3) {
        this.unknown3 = unknown3;
        return this;
    }

    public String getUnknown4() {
        return unknown4;
    }

    public ZdpRecord setUnknown4(String unknown4) {
        this.unknown4 = unknown4;
        return this;
    }

    public String getUnknown5() {
        return unknown5;
    }

    public ZdpRecord setUnknown5(String unknown5) {
        this.unknown5 = unknown5;
        return this;
    }

    public Double getDegrees() {
        return degrees;
    }

    public ZdpRecord setDegrees(Double degrees) {
        this.degrees = degrees;
        return this;
    }

    public String getUnknown6() {
        return unknown6;
    }

    public ZdpRecord setUnknown6(String unknown6) {
        this.unknown6 = unknown6;
        return this;
    }

    public String getUnknown7() {
        return unknown7;
    }

    public ZdpRecord setUnknown7(String unknown7) {
        this.unknown7 = unknown7;
        return this;
    }
}
