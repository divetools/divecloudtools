package com.gratteburnes.divetools.divecloud.parse.model.zar;

public enum GearFields {
    GEAR_UNITS,
    NAME,
    WEIGHTBELT,
    SUIT
}
