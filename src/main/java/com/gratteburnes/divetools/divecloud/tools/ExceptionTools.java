package com.gratteburnes.divetools.divecloud.tools;

public class ExceptionTools {
    public static void illegal(String message) {
        throw new IllegalArgumentException(message);
    }
    public static void runtime(String message) {
        throw new RuntimeException(message);
    }
}
