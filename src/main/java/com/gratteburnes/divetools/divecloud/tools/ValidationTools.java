package com.gratteburnes.divetools.divecloud.tools;

import org.apache.commons.lang3.StringUtils;

import static com.gratteburnes.divetools.divecloud.tools.ExceptionTools.illegal;

public class ValidationTools {
    public static void validateNotBlank(String value) {
        if(StringUtils.isBlank(value)) {
            illegal("value cannot be blank");
        }
    }

    public static void validateHeader(String actual, String expected) {
        if(StringUtils.isBlank(actual)) {
            illegal("header cannot be blank");
        }
        if(!actual.equals(expected)) {
            illegal("header is invalid: " + actual + " : " + expected);
        }
    }
}
