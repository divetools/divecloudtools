package com.gratteburnes.divetools.divecloud.tools;

import com.gratteburnes.divetools.divecloud.acquire.model.response.AbstractResponse;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

import static com.gratteburnes.divetools.divecloud.tools.ExceptionTools.illegal;

public class ResponseTools {
    public static final String TOKEN_START = "{";
    public static final String TOKEN_END = "}";

    public static String convertResponseToEnumName(String string) {
        return string == null ? "NULL": string.trim().replace(" ", "_");
    }

    public static String convertResponseFromEnumName(String string) {
        return string == null ? "": string.replace("_", " ");
    }

    public static void validate(AbstractResponse resp, String token) {
        if(resp == null) {
            illegal("Response cannot be null");
        }
        if(StringUtils.isBlank(token)) {
            illegal("Token cannot be blank");
        }
    }

    public static String[] tokenizeSource(String source) {
        if(StringUtils.isBlank(source)) {
            illegal("Source cannot be blank");
        }

        // payload may come newline terminated
        String work = source.replaceAll("\\n", "");

        if(!work.startsWith(TOKEN_START) || !work.endsWith(TOKEN_END)) {
            illegal("Invalid data format: '" + work+"'");
        }

        work = work.replaceAll(Pattern.quote(TOKEN_START), "");
        return work.split(Pattern.quote(TOKEN_END));
    }
}
